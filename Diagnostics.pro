TEMPLATE = app

QT += qml quick widgets network xml

SOURCES += src/main.cpp \
    src/settings.cpp \
    src/socketinterface.cpp \
    src/worker.cpp

RESOURCES += \
    resource/icons.qrc \
    resource/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += inc/settings.h \
    inc/socketinterface.h \
    inc/worker.h \
    inc/RequestMessages.h \
    inc/ResponseMessages.h

DISTFILES += \
    settings.xml \
    resource/Help.txt

#RC_FILE = appicon.rc

CONFIG(release, debug|release) {
    DESTDIR = release
} else {
    DESTDIR = debug
}

#for Windows
win32 {
    #in Windows, you can use & to separate commands
    copyfiles.commands += @echo NOW COPYING ADDITIONAL FILE(S) &
    copyfiles.commands += @call copy ..\\$${TARGET}\\resource\\Help.txt $${DESTDIR}\\Help.txt
}

#for Mac
macx {
     #commands would go here if I had them
}

QMAKE_EXTRA_TARGETS += copyfiles
POST_TARGETDEPS += copyfiles

TARGET = DoIP_AE

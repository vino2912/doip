#ifndef SOCKETINTERFACE_H
#define SOCKETINTERFACE_H

#include <QThread>
#include <inc/worker.h>

#define PORT_UDP 64404
#define PORT_TCP 64403

typedef unsigned char u_int8_t;
typedef unsigned short u_int16_t;
typedef struct
{
    u_int8_t Message_ID;
    u_int8_t Message_Data1;
    u_int16_t Message_Data2;
} CAN_ETHERNET_DATA;

class SocInterface : public QObject
{
    Q_OBJECT
    public:
        SocInterface(QObject *parent = 0);
        ~SocInterface();
        Q_PROPERTY(QString updateStatus READ status WRITE setStatus NOTIFY updateStatus)
        Q_PROPERTY(bool connectionStatus READ connectionStatus WRITE setConnectionStatus NOTIFY connectionStatusChanged)

        Q_SIGNAL void dataReceived(QVariant status);
        Q_SIGNAL void networkNameListChanged(QStringList sList);
signals:
    void updateStatus(QString text);
    void connectionStatusChanged(bool status);

    //Signals to worker thread
    void sendRequestSignal(QString);
    void sendSeatHeaterValueSignal(QString);
    void setTgtIPSignal(QString);
    void setTesterAddressSignal(QString);
    void setDoIPNodeAddressSignal(QString);
    void setPortNumSignal(quint16, quint8);
    void connectToTargetSignal();
    void disconnectFromTargetSignal();
    void setWriteDIDValueSignal(QString);

public slots:
    Q_INVOKABLE QString sendRequest(QString request);
    Q_INVOKABLE QString sendSeatHeaterValue(QString value);
    Q_INVOKABLE bool checkIP(QString newIP);
    Q_INVOKABLE QString setHostIP(QString, QString);
    Q_INVOKABLE QString setTgtIP(QString);
    Q_INVOKABLE QString setTesterAddress(QString);
    Q_INVOKABLE QString setDoIPNodeAddress(QString);
    Q_INVOKABLE void setPortNum(quint16 portNum, quint8 protocol);
    Q_INVOKABLE QString connectToTarget();
    Q_INVOKABLE void disconnectFromTarget();
    Q_INVOKABLE void setResetIP(int reset);
    Q_INVOKABLE QString getInterfaceIP(const QString sName = "");
    Q_INVOKABLE void getSystemIP();
    Q_INVOKABLE void getAvailableLANConnections();
    Q_INVOKABLE void setWriteDIDValue(QString ba);
    void setStatus(QString text);
    QString status();
    void setConnectionStatus(bool status);
    bool connectionStatus();

private:
    QMap<QString, QString> hostIP;
    QString m_networkConnectionName;
    QString m_LastLog;
    QMap<QString, QString> m_presentIP;
    bool m_ConnectionStatus;
    bool m_resetIPOnExit;
    QThread workerThread;
    QStringList m_networkNameList;
};

#endif // SOCKETINTERFACE_H

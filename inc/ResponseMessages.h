#ifndef RESPONSE_MESSAGES_H
#define RESPONSE_MESSAGES_H

/* Vehicle Identification Response */
//char VID_Response[] =
//{
//		/* Protocol version DoIP 2012 and its inversion (2B) */
//		0x02, 0xFD,
//		/* Payload Type (2B) = Vehicle Identification Response */
//		0x00, 0x04,
//		/* Payload Length (4B) */
//		0x00, 0x00, 0x00, 0x21,
//		/* Payload Specific */
//		/* VIN (17 Bytes) */
//		0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,
//		/* ECU logical address (2B) */
//		0x00,0x01,
//		/* Entity IDetification (6B) */
//		0x00,0x00,0x00,0x00,0x01,0xF4,
//		/* GID (6) */
//		0x00,0x00,0x00,0x00,0x00,0x00,
//		/* Additional Info */
//		0x00,0x00
//};

/* Routing Activation Response */
//char RA_Response[] =
//{
//		/* Protocol version DoIP 2012 and its inversion (2B) */
//		0x02, 0xFD,
//		/* Payload Type (2B) = Routing Activation Response*/
//		0x00, 0x06,
//		/* Payload Length (4B) */
//		0x00, 0x00, 0x00, 0x09,
//		/* Payload Specific */
//		/* Address of test equipment (2B)*/
//		0x00,0x64,
//		/* ECU logical address (2B) */
//		0x00, 0x01,
//		/* Routing activation result (1B) = Success */
//		0x10,
//		/* Reserved (8B) */
//		0x00, 0x00, 0x00, 0x00/*, 0x00, 0x00, 0x00, 0x00*/
//};

/* Diagnostic Positive Acknowledgment */
char Diag_Ack [] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic Positive Acknowledgment */
		0x80, 0x02,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x05,
		/* Payload Specific */
		/* SA (2B), Address of test equipment, same as used in diagnostic sessions*/
		0x00 ,0xC8,
		/* TA (2B), Address of the ECU */
		0x00,0x64,
		/* Positive Ack Code (1B) = 0 */
		0x00
};

/* Generic Negative Acknowledgment (Wrong Protocol version) */
char Generic_negativeAck_wrongPrtocolVersion [] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Generic Negative Acknowledgment */
		0x00, 0x00,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x01,
		/* Payload Specific */
		/* Negative Ack Code (1B) = 0 */
		0x00
};

/* Generic Negative Acknowledgment (Not supported payload type) */
char Generic_negativeAck_notSupportedPayloadType [] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Generic Negative Acknowledgment */
		0x00, 0x00,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x01,
		/* Payload Specific */
		/* Negative Ack Code (1B) = 1 */
		0x01
};

/* Generic Negative Acknowledgment (Long payload) */
char Generic_negativeAck_longPayload [] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Generic Negative Acknowledgment */
		0x00, 0x00,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x01,
		/* Payload Specific */
		/* Negative Ack Code (1B) = 2 */
		0x02
};

/* Generic Negative Acknowledgment (Payload length mismatch) */
char Generic_negativeAck_payloadLengthMismatch [] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Generic Negative Acknowledgment */
		0x00, 0x00,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x01,
		/* Payload Specific */
		/* Negative Ack Code (1B) = 2 */
		0x04
};

/* Current Diagnostic Session Default Response */
char CurrentDiagSessionDefault_Response[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x08,
		/* Payload Specific */
		/* SA (2B) */
		0x00 ,0xC8,
		/* TA (2B) */
		0x00,0x64,
		/* Actual diagnostic message*/
		/* Service ID = ReadDataByIdentifier Response */
		0x62,
		/* Data Identifier */
		0xF1, 0x86,
		/* Data Record */
		0x01
};

/* Diagnostic Message - Current Diagnostic Extended Session Response */
char CurrentDiagSessionExtended_Response[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x08,
		/* Payload Specific */
		/* SA (2B) */
		0x00 ,0xC8,
		/* TA (2B) */
		0x00,0x64,
		/* Actual diagnostic message*/
		/* Service ID = ReadDataByIdentifier Response */
		0x62,
		/* Data Identifier */
		0xF1, 0x86,
		/* Data Record */
		0x03
};

char RequestExtendedSes_Response[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x0A,
		/* Payload Specific */
		/* SA (2B) */
		0x00 ,0xC8,
		/* TA (2B) */
		0x00,0x64,
		/* Actual diagnostic message*/
		/* Service ID = DiagnosticSessionControl Response  */
		0x50,
		/* diagnosticSessionType = Default Session */
		0x03,
		/* Session parameter record */
		0x00, 0x32, 0x01, 0xF4
};

char ReadDID_Response_0[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x08,
		/* Payload Specific */
		/* SA (2B) */
		0x00 ,0xC8,
		/* TA (2B) */
		0x00,0x64,
		/* Actual diagnostic message*/
		/* Service ID = ReadDataByIdentifier Response */
		0x62,
		/* Data Identifier */
		0x00, 0x00,
		/* Data Record */
		0x00
};

char ReadDID_Response_10[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x08,
		/* Payload Specific */
		/* SA (2B) */
		0x00 ,0xC8,
		/* TA (2B) */
		0x00,0x64,
		/* Actual diagnostic message*/
		/* Service ID = ReadDataByIdentifier Response */
		0x62,
		/* Data Identifier */
		0x00, 0x00,
		/* Data Record */
		0x10
};

char WriteDID_Response[] = {\
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x07,
		/* Payload Specific */
		/* SA (2B) */
		0x00 ,0xC8,
		/* TA (2B) */
		0x00,0x64,
		/* Actual diagnostic message*/
		/* Service ID = WriteDataByIdentifier Response */
		0x6E,
		/* Data Identifier */
		0x00, 0x00
};

/* Diagnostic Power Mode Information Response */
char DiagPwMdInfo_Response[] = {\
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic Power Mode Information Response */
		0x40, 0x04,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x01,
		/* Power mode status (1B) */
		0x01
};

/* Entity Status Request */
char DiagEntityStatus_Response[] = {\
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Entity Status Request */
		0x40, 0x02,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x07,
		/* Node type = DoIP node (1B) */
		0x01,
		/* Maximum number of TCP_DATA sockets (1B) */
		0x01, /* Shall be taken from configuration when supported in the code */
		/* Number of currently established sockets (1B) */
		0x01,
		/* Maximum size of request that ECU can process (4B) */
		0x00, 0x00, 0x00, 0x1E /* Shall be taken from configuration when supported in the code */
};

#endif /* RESPONSE_MESSAGES_H */

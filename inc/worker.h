#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QUdpSocket>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QProcess>

class Worker : public QObject
{
    Q_OBJECT
    public:
        Worker(QObject *parent = 0);
        ~Worker();

signals:
    void updateStatus(QString text);
    void connectionStatusChanged(bool status);
    void dataReceived(QVariant status);
    void operate(QTcpSocket *, quint16 tcpPortNum, QString targetIP, quint16 tgtPortNum);

public slots:
    void tcpReadData();
    void udpReadData();
    void socketConnected();
    void socketDisconnected();
    void bytesWritten(qint64 bytes);
    void sendRequest(QString request);
    void sendSeatHeaterValue(QString value);
    void setTgtIP(QString);
    void setTesterAddress(QString);
    void setDoIPNodeAddress(QString);
    void setPortNum(quint16 portNum, quint8 protocol);
    void connectToTarget();
    void disconnectFromTarget();
    void setWriteDIDValue(QString ba);
    bool getHexBytesFromHexString(QString str, quint8 *result, qint16 result_array_len);

private:
    void printFrame(const void *buffer, int buffer_len);
    int AssertResponse (char * received, char * Expected, int len);
    QString targetIP;
    QUdpSocket *udpsocket;
    QTcpSocket *tcpsocket;
    QHostAddress target;
    quint16 udpPortNum;
    quint16 tcpPortNum;
    quint16 tgtPortNum;
    quint16 seatHeaterModePortNum;
    quint16 seatHeaterTempPortNum;
    quint8 m_aWriteDIDVal[2];
    quint8 m_aTesterAddr[2];
    quint8 m_aDoIPNodeAddr[2];
    QStringList sList;
    bool m_ConnectionStatus;
};
#endif // WORKER_H


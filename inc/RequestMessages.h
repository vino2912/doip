#ifndef REQUEST_MESSAGES_H
#define REQUEST_MESSAGES_H

/* Routing Activation Request */
char RA_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Routing Activation Request*/
		0x00, 0x05,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x0B,
		/* Payload Specific */
		/* SA (2B), Address of test equipment, same as used in diagnostic sessions*/
		0x00,0x64,
		/* Type of routing activation */
		0x00,
		/* Reserved */
		0x00, 0x00, 0x00, 0x00,
		/* Reserved */
		0x00, 0x00, 0x00, 0x00
};

/* Diagnostic Message - Current Session Request */
char CurrentDiagSession_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x07,
		/* Payload Specific */
		/* SA (2B), Address of test equipment, same as used in diagnostic sessions*/
		0x00,0x64,
		/* TA (2B), Address of the ECU */
		0x00 ,0xC8,
		/* Actual diagnostic message*/
		/* Service ID = ReadDataByIdentifier */
		0x22,
		/* Data Identifier */
		0xF1, 0x86
};

/* Diagnostic Message - Extended Session Request */
char RequestExtendedSes_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x06,
		/* Payload Specific */
		/* SA (2B), Address of test equipment, same as used in diagnostic sessions*/
		0x00,0x64,
		/* TA (2B), Address of the ECU */
		0x00 ,0xC8,
		/* Actual diagnostic message*/
		/* Service ID = extendedDiagnosticSession */
		0x10,
		/* Session Type = Extended */
		0x03
};

/* Diagnostic Message - Read DID Request */
char ReadDID_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x07,
		/* Payload Specific */
		/* SA (2B), Address of test equipment, same as used in diagnostic sessions*/
		0x00,0x64,
		/* TA (2B), Address of the ECU */
		0x00 ,0xC8,
		/* Actual diagnostic message*/
		/* Service ID = ReadDataByIdentifier */
		0x22,
		/* Data Identifier */
		0x00, 0x00
};

/* Diagnostic Message - Write DID Request */
char WriteDID_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic message */
		0x80, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x08,
		/* Payload Specific */
		/* SA (2B), Address of test equipment, same as used in diagnostic sessions*/
		0x00,0x64,
		/* TA (2B), Address of the ECU */
		0x00 ,0xC8,
		/* Actual diagnostic message*/
		/* Service ID = WriteDataByIdentifier */
		0x2E,
		/* Data Identifier */
		0x00, 0x00,
		/* Data Record */
		0x10
};

/* Vehicle Identification Request Message */
char VID_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Vehicle Identification Request */
		0x00, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x00,
};

/* Vehicle Identification Request with EID */
char VIDwEID_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Vehicle Identification Request with EID */
		0x00, 0x02,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x06,
		/* Payload Specific */
		/* EID (6B) */
		0x00, 0x00, 0x00, 0x00, 0x01, 0xF4
};

/* Vehicle Identification Request with VIN */
char VIDwVIN_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Vehicle Identification Request with VIN */
		0x00, 0x03,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x11,
		/* Payload Specific */
		/* VIN (17B) */
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11
};

/* Vehicle Identification Request with wrong protocol version Message */
char VIDwWrongProtocolVersion_Req[] =
{
		/* Protocol version (wrong) (2B) */
		0x07, 0xFD,
		/* Payload Type (2B) = Vehicle Identification Request */
		0x00, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x00,
};

/* Invalid payload type Message */
char InvalidPayloadType_Req[] =
{
		/* Protocol version (wrong) (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Not supported type */
		0x00, 0xFF,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x00,
};

/* Long payload Message */
char LongPayload_Req[] =
{
		/* Protocol version (wrong) (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) */
		0x00, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x40,
		/* Data */
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
};

/* Invalid payload type Message */
char PayloadLengthMismatch_Req[] =
{
		/* Protocol version (wrong) (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Not supported type */
		0x00, 0x02,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x05,
		/* Payload */
		0x00, 0x00, 0x00, 0x00, 0x01,
};

/* Diagnostic Power Mode Information Request */
char DiagPwMdInfo_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Diagnostic Power Mode Information Request */
		0x40, 0x03,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x00
};

/* Entity Status Request */
char DiagEntityStatus_Req[] =
{
		/* Protocol version DoIP 2012 and its inversion (2B) */
		0x02, 0xFD,
		/* Payload Type (2B) = Entity Status Request */
		0x40, 0x01,
		/* Payload Length (4B) */
		0x00, 0x00, 0x00, 0x00
};



#endif /* REQUEST_MESSAGES_H */

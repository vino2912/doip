#ifndef SETTINGS
#define SETTINGS

#include <QtXml>

#define HELP_FILE "Help.txt"
class DiagSettings : public QObject
{
    Q_OBJECT
    public:
        DiagSettings(QObject *parent = 0);
        ~DiagSettings();
        Q_PROPERTY(QVariant diagSettings READ diagSettings WRITE setDiagSettings NOTIFY diagSettingsChanged)
        Q_PROPERTY(int connectType READ connectType WRITE setConnectType NOTIFY connectTypeChanged)
        Q_PROPERTY(int resetIP READ resetIP WRITE setResetIP NOTIFY resetIPChanged)
        Q_PROPERTY(int appWidth READ appWidth WRITE setAppWidth NOTIFY appWidthChanged)
        Q_PROPERTY(int appHeight READ appHeight WRITE setAppHeight NOTIFY appHeightChanged)

        QVariant diagSettings() {
            return m_diagSettings;
        }

        int connectType() {
            return m_connectType;
        }

        int resetIP() {
            return m_resetIP;
        }

        int appWidth() {
            return m_appWidth;
        }

        int appHeight() {
            return m_appHeight;
        }
signals:
        void diagSettingsChanged();
        void appWidthChanged();
        void appHeightChanged();
        void connectTypeChanged();
        void resetIPChanged();

public slots:
        Q_INVOKABLE void setDiagSettings(QVariant updatedSettings);
        Q_INVOKABLE void setConnectType(int connectType);
        Q_INVOKABLE QString saveLog(QString file, QString text);
        Q_INVOKABLE void setResetIP(int reset);
        Q_INVOKABLE QString getHelpText();
        void setAppWidth(int width);
        void setAppHeight(int height);

private:
        QFile m_xmlFile;
        QVariant m_diagSettings;
        QDomElement m_hostIp;
        QDomElement m_udpPort;
        QDomElement m_tcpPort;
        QDomElement m_tgtIP;
        QDomElement m_tgtPort;
        QDomDocument m_document;
        QDomElement m_testerAddr;
        QDomElement m_doipNodeAddr;
        QDomElement m_shModePort;
        QDomElement m_shTempPort;
        QDomElement m_networkName;
        int m_appHeight;
        int m_appWidth;
        int m_connectType;
        int m_resetIP;
};
#endif // SETTINGS


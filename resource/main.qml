import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import com.mgc.evb.socintf 1.0
import com.mgc.evb.diagsettings 1.0

ApplicationWindow {
    title: qsTr("DoIP/Automotive Ethernet")
    width: settingintf.appWidth
    height: settingintf.appHeight
    visible: true
    flags: Qt.Dialog | Qt.WindowTitleHint | Qt.WindowCloseButtonHint
    color: "#143D66"
    onClosing: {
        console.log("session end")
    }

    SocIntf {
        id: socintf
    }

    DiagSettings {
        id: settingintf
    }

    //    menuBar: MenuBar {
    //        Menu {
    //            title: qsTr("&File")
    //            MenuItem {
    //                text: qsTr("&Open")
    //                onTriggered: messageDialog.show(qsTr("Open action triggered"));
    //            }
    //            MenuItem {
    //                text: qsTr("E&xit")
    //                onTriggered: Qt.quit();
    //            }
    //        }
    //    }

    MainForm {
        id: doipMainForm
        anchors.fill: parent
        homeTab.onClicked: {
            diagTab.selected = false
            seatHeaterTab.selected = false
            settingsTab.selected = false
            abtTab.selected = false
            helpTab.selected = false
        }
        settingsTab.onClicked: {
            diagTab.selected = false
            seatHeaterTab.selected = false
            homeTab.selected = false
            abtTab.selected = false
            helpTab.selected = false
        }
        diagTab.onClicked: {
            settingsTab.selected = false
            seatHeaterTab.selected = false
            homeTab.selected = false
            abtTab.selected = false
            helpTab.selected = false
        }
        seatHeaterTab.onClicked: {
            settingsTab.selected = false
            diagTab.selected = false
            homeTab.selected = false
            abtTab.selected = false
            helpTab.selected = false
        }
        helpTab.onClicked: {
            settingsTab.selected = false
            seatHeaterTab.selected = false
            homeTab.selected = false
            abtTab.selected = false
            diagTab.selected = false
        }
        abtTab.onClicked: {
            settingsTab.selected = false
            seatHeaterTab.selected = false
            homeTab.selected = false
            helpTab.selected = false
            diagTab.selected = false
        }
        updateSettingBtn.onClicked: {
            var retVal = socintf.checkIP(newIP.text);
            if(!retVal)
            {
                messageDialog.show(qsTr("Invalid Host IP"));
                return;
            }
            retVal = socintf.checkIP(tgtIP.text);
            if(!retVal)
            {
                messageDialog.show(qsTr("Invalid Target IP"));
                return;
            }

            if(!enableSaveSetting) {
                if(socintf.connectionStatus) {
                    messageDialog.show(qsTr("Edit is not allowed when tool is in connected state"))
                    return;
                }
                enableSaveSetting = true
                return;
            }
            else {
                enableSaveSetting = false
            }

            retVal = socintf.setHostIP(newIP.text, networkConnectionName);
            printTrace(retVal);

            retVal = socintf.setTgtIP(tgtIP.text);
            printTrace(retVal);

            socintf.setPortNum(udpPortNum.text, 0);
            socintf.setPortNum(tcpPortNum.text, 1);
            socintf.setPortNum(tgtPortNum.text, 2);
            socintf.setPortNum(seatHeaterModePort, 3);
            socintf.setPortNum(seatHeaterTempPort, 4);

            retVal = socintf.setTesterAddress(testerAddress);
            printTrace(retVal);

            retVal = socintf.setDoIPNodeAddress(doipNodeAddress);
            printTrace(retVal);

            var newSettings = new Object();
            newSettings["hostIP"] = newIP.text;
            newSettings["udpPort"] = udpPortNum.text;
            newSettings["tcpPort"] = tcpPortNum.text;
            newSettings["width"] = appWidth.text;
            newSettings["height"] = appHeight.text;
            newSettings["tgtIP"] = tgtIP.text;
            newSettings["tgtPort"] = tgtPortNum.text;
            newSettings["testerAddr"] = testerAddress;
            newSettings["doipNodeAddr"] = doipNodeAddress;
            newSettings["doipNodeAddr"] = doipNodeAddress;
            newSettings["seatHeaterModePort"] = seatHeaterModePort;
            newSettings["seatHeaterTempPort"] = seatHeaterTempPort;
            newSettings["networkConnection"] = networkConnectionName;
            settingintf.setDiagSettings(newSettings);
        }
        quitButton.onClicked: {
            quitButton.selected = false
            quitDialog.show(qsTr("Do you really want to exit?"))
        }
        saveBtn.onClicked: {
            fileDialog.open();
        }
        clearBtn.onClicked: {
            clearDialog.show(qsTr("Do you want to clear the traces?"))
        }

        setSeatHeaterBtn.onClicked: {
            if(!socintf.connectionStatus) {
                messageDialog.show(qsTr("Target Not Connected"))
                return;
            }
            var retval  = socintf.sendSeatHeaterValue(seatHeaterValue);
            printTrace(retval);
        }

        sendRequestBtn.onClicked: {
            if(!socintf.connectionStatus) {
                messageDialog.show(qsTr("Target Not Connected"))
                return;
            }

            socintf.setWriteDIDValue(writeDIDVal)
            diagAckPVText = ""
            diagAckIPVText = ""
            diagAckTypeText = ""
            diagAckLenText = ""
            diagAckDataText = ""
            diagAckStatusColor = "#b8cbde"
            diagAckStatusText = ""
            reqPVText = ""
            reqIPVText = ""
            reqTypeText = ""
            reqLenText = ""
            reqDataText = ""
            reqStatusColor = "#b8cbde"
            reqStatusText = ""
            responsePVText = ""
            responseIPVText = ""
            responseTypeText = ""
            responseLenText = ""
            responseDataText = ""
            responseStatusColor = "#b8cbde"
            responseStatusText = ""
            printTrace(socintf.sendRequest(requestType))
        }

        connectBtn.onClicked: {
            if(1 == connectionStatus) {
                socintf.disconnectFromTarget();
            }
            else {
                var retval = socintf.connectToTarget();
                printTrace(retval);
            }
        }

        connectType.onClicked: {
            printTrace("Connect Automatically after startup : " + (connectType.checked? "Enabled": "Disabled"));
            settingintf.setConnectType(connectType.checked);
        }

        resetIpOnExit.onClicked: {
            printTrace("Reset IP on exit : " + (resetIpOnExit.checked? "Enabled": "Disabled"));
            settingintf.setResetIP(resetIpOnExit.checked);
            socintf.setResetIP(resetIpOnExit.checked);
        }

        Component.onCompleted: {
            target: settingintf
            onDiagSettingsChanged: {
                var setting = settingintf.diagSettings
                newIP.text = setting["hostIP"];
                udpPortNum.text = setting["udpPort"];
                tcpPortNum.text = setting["tcpPort"];
                appWidth.text = setting["width"];
                appHeight.text = setting["height"];
                tgtIP.text = setting["tgtIP"];
                tgtPortNum.text = setting["tgtPort"];
                testerAddress = setting["testerAddr"];
                doipNodeAddress = setting["doipNodeAddr"];
                seatHeaterModePort = setting["seatHeaterModePort"];
                seatHeaterTempPort = setting["seatHeaterTempPort"];

                var retVal = socintf.setTgtIP(tgtIP.text);
                printTrace(retVal);

                socintf.setPortNum(udpPortNum.text, 0);
                socintf.setPortNum(tcpPortNum.text, 1);
                socintf.setPortNum(tgtPortNum.text, 2);
                socintf.setPortNum(seatHeaterModePort, 3);
                socintf.setPortNum(seatHeaterTempPort, 4);

                retVal = socintf.setTesterAddress(testerAddress);
                printTrace(retVal);

                retVal = socintf.setDoIPNodeAddress(doipNodeAddress);
                printTrace(retVal);
            }

            onConnectTypeChanged: {
                connectType.checked = settingintf.connectType;
                if(1 == settingintf.connectType)
                {
                    var retval = socintf.connectToTarget();
                    printTrace(retval);
                }
            }

            onResetIPChanged: {
                resetIpOnExit.checked = settingintf.resetIP;
                socintf.getSystemIP();
                socintf.getAvailableLANConnections();
                socintf.setResetIP(resetIpOnExit.checked);
            }
            onHelpUpdate: {
               helpText = settingintf.getHelpText();
            }
        }

        function printTrace(text) {
            if(text == "")
                return;
            tracetext.text = tracetext.text + new Date().toLocaleTimeString() + ": " + text + "\n";
        }
        function clearTrace() {
            tracetext.text = ""
        }
        function setTraceFile(fileName) {
            var retVal = settingintf.saveLog(fileName, tracetext.text);
            printTrace("Log file saved to : " + retVal);
        }
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("Message - DoIP")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
    MessageDialog {
        id: quitDialog
        title: qsTr("Message - DoIP")
        standardButtons:  StandardButton.Yes | StandardButton.No;

        function show(caption) {
            quitDialog.text = caption;
            quitDialog.open();
        }
        onYes: {
            Qt.quit()
        }
    }
    MessageDialog {
        id: clearDialog
        property bool yesPressed: false
        title: qsTr("Message - DoIP")
        standardButtons:  StandardButton.Yes | StandardButton.No;

        function show(caption) {
            clearDialog.text = caption;
            clearDialog.open();
        }
        onYes: {
            doipMainForm.clearTrace()
        }
    }
    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: curFolder
        selectExisting: false
        property string curFolder: shortcuts.home
        onAccepted: {
            fileDialog.curFolder = fileDialog.folder
            doipMainForm.setTraceFile(fileDialog.fileUrls);
        }
        onRejected: {
            console.log("Canceled")
        }
    }
}

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.2
import QtQuick.Window 2.2

Item {
    width: 1280
    height: 800

    property alias homeTab: homeTab
    property alias settingsTab: settingsTab
    property alias diagTab: diagTab
    property alias saveBtn: saveBtn
    property alias clearBtn: clearBtn
    property alias helpTab: helpTab
    property alias abtTab: abtTab
    property alias seatHeaterTab: seatHeaterTab
    property alias sendRequestBtn: sendRequestBtn
    property alias requestType: requestType.currentText
    property alias setSeatHeaterBtn: setSeatHeaterBtn
    property alias seatHeaterValue: seatHeaterCBox.currentText
    property alias networkConnectionName: netNameCBox.currentText
    property alias quitButton: quitButton
    property alias updateSettingBtn: updateSettingBtn
    property alias connectType: connectTypeChkBox
    property alias resetIpOnExit: ipResetChkBox
    property alias connectBtn: connectBtn
    property alias newIP: textEdit1
    property alias udpPortNum: textEdit3
    property alias tcpPortNum: textEdit2
    property alias appWidth: textEdit4
    property alias appHeight: textEdit5
    property alias tgtIP: textEdit7
    property alias tgtPortNum: textEdit9
    property alias seatHeaterModePort: textEdit10.text
    property alias seatHeaterTempPort: textEdit11.text
    property alias testerAddress: textEdit6.text
    property alias doipNodeAddress: textEdit8.text
    property alias tracetext: tracetext
    property alias helpText: helpText.text
    property bool connectionStatus: false
    property alias enableSaveSetting: updateSettingBtn.saveMode
    property alias diagAckPVText: diagAckPV.text
    property alias diagAckIPVText: diagAckIPV.text
    property alias diagAckTypeText: diagAckType.text
    property alias diagAckLenText: diagAckLen.text
    property alias diagAckDataText: diagAckData.text
    property alias diagAckStatusColor: diagAckStatusRect.color
    property alias diagAckStatusText: diagAckStatus.text
    property alias reqPVText: reqPV.text
    property alias reqIPVText: reqIPV.text
    property alias reqTypeText: reqType.text
    property alias reqLenText: reqLen.text
    property alias reqDataText: reqData.text
    property alias reqStatusColor: reqStatusRect.color
    property alias reqStatusText: reqStatus.text
    property alias responsePVText: responsePV.text
    property alias responseIPVText: responseIPV.text
    property alias responseTypeText: responseType.text
    property alias responseLenText: responseLen.text
    property alias responseDataText: responseData.text
    property alias responseStatusColor: responseStatusRect.color
    property alias responseStatusText: responseStatus.text
    property alias writeDIDVal: writeDIDVal.text
    property var networkNameList: [""]

    ComboBox {
        id: requestType
        x: Math.max(205, ((parent.width - 200) / 2) + 100 - 160)
        y: 100
        visible: diagTab.selected == true
        width: 320
        height: 38
        model: ["ReadDID_Req", "WriteDID_Req", "Current_DiagSession_Req", "ExtendedSession_Req", "PowerModeInfo_Req", "EntityStatus_Req",
            "LongPayload_Req", "PayloadLengthMismatch_Req", "InvalidPayloadType_Req", "VIDwWrongProtocolVersion_Req"]
        style: ComboBoxStyle {
            background: Rectangle {
                color: "#b8cbde"
                Image {
                    anchors.right: parent.right
                    width: 20
                    height: 20
                    anchors.verticalCenter: parent.verticalCenter
                    source: "arrow_down.png"
                }
            }
            label: Text {
                width: 200
                height: 50
                text: qsTr(control.currentText)
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 12
            }
        }
    }
    Rectangle {
        id: writeDIDRect
        visible: (requestType.currentText == "WriteDID_Req") && (diagTab.selected == true)
        width: requestType.currentText == "WriteDID_Req"? 100 : 0
        anchors.left: requestType.right
        anchors.leftMargin: 20
        anchors.verticalCenter: requestType.verticalCenter
        height: 38
        color: "#ffffff"
        border.width: 1
        TextField {
            id: writeDIDVal
            anchors.fill: parent
            inputMask: "\\0\\xHh \\0\\xHh"
            font.pixelSize: 18
            focus: requestType.currentText == "WriteDID_Req"? true : false
            text: "0x00 0x10"
        }
    }

    MyButton {
        id: sendRequestBtn
        visible: requestType.visible
        anchors.left: writeDIDRect.right
        anchors.leftMargin: 20
        anchors.verticalCenter: requestType.verticalCenter
        btnwidth: 120
        btnheight: 40
        btnRadius: 5
        btnColor: "#FFCC00"
        gradientColor: "#CCA300"
        textColor: "#000000"
        text: qsTr("Send")
        textSize: 26
    }

    Item {
        id: dataTbl
        width: 1050
        height: 360
        visible: requestType.visible
        anchors.top: sendRequestBtn.bottom
        anchors.topMargin: 30
        x: Math.max(205, ((parent.width - 200) / 2) + 100 - 500)
        Rectangle {
            id: borderRect
            color: "#00FF00"
            width: 1050
            height: 360
            border.width: 2
            border.color: "#000000"

            GridLayout {
                width: parent.width
                height: parent.height
                columns: 7
                rowSpacing: 0
                columnSpacing: 0
                Rectangle {
                    width: 100
                    height: 72
                    border.width: 1
                    color: "#7D0032"
                    border.color: "#ffffff"
                }
                Rectangle {
                    width: 100
                    height: 72
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        color: "#ffffff"
                        text: "Protocol Version"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 100
                    height: 72
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Inverse Protocol Version"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 100
                    height: 72
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Payload Type"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 100
                    height: 72
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Payload Length"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 475
                    height: 72
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Payload Data"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 75
                    height: 72
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Status"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }

                Rectangle {
                    width: 100
                    height: 96
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Request"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: reqPV
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: reqIPV
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: reqType
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: reqLen
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 475
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: reqData
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    id: reqStatusRect
                    width: 75
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: reqStatus
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }

                Rectangle {
                    width: 100
                    height: 96
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Diagnostic Ack."
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: diagAckPV
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: diagAckIPV
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: diagAckType
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: diagAckLen
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 475
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: diagAckData
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    id: diagAckStatusRect
                    width: 75
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: diagAckStatus
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    color: "#7D0032"
                    border.width: 1
                    border.color: "#ffffff"
                    Text {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: "Response"
                        color: "#ffffff"
                        font.pixelSize: 18
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: responsePV
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: responseIPV
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: responseType
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 100
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: responseLen
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    width: 475
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: responseData
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
                Rectangle {
                    id: responseStatusRect
                    width: 75
                    height: 96
                    border.width: 1
                    border.color: "#ffffff"
                    color: "#b8cbde"
                    Text {
                        id: responseStatus
                        anchors.fill: parent
                        anchors.centerIn: parent
                        text: ""
                        font.pixelSize: 16
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                        elide: Text.ElideRight
                    }
                }
            }
        }
    }

    Rectangle {
        id: rectangle3
        x: 201
        y: 440
        width: parent.width - 203
        height: parent.height - 540
        radius: 6
        border.width: 1
        color: "#b8cbde"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 2
        Rectangle {
            width: 125
            height: 25
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 1
            color: "#000000"
            border.width: 1
            border.color: "#b8cbde"
            Text {
                text: qsTr("Trace Messages")
                color: "#FFFFFF"
                font.pixelSize: 15
                anchors.centerIn: parent
            }
        }
        ScrollView {
            id: traceView
            width: parent.width
            height: parent.height - 30
            //            anchors.fill: parent
            anchors.bottom: parent.bottom
            horizontalScrollBarPolicy: Qt.ScrollBarAsNeeded
            verticalScrollBarPolicy: Qt.ScrollBarAsNeeded
            TextEdit {
                id: tracetext
                x: 2
                y: 2
                //                color: "#4b3d0b"
                color: "black"
                text: qsTr("")
                font.family: "Arial"
                wrapMode: Text.WordWrap
                readOnly: true
                selectByMouse: true
                font.pointSize: 10
            }
        }
        MyButton {
            id: saveBtn
            anchors.top: parent.top
            anchors.topMargin: 1
            anchors.left: clearBtn.right
            anchors.leftMargin: 2
            btnwidth: 100
            btnheight: 25
            btnRadius: 0
            btnColor: "#FFCC00"
            gradientColor: "#CCA300"
            textColor: "#000000"
            text: qsTr("Export Trace")
            textSize: 15
        }
        MyButton {
            id: clearBtn
            anchors.top: parent.top
            anchors.topMargin: 1
            x: Math.max(160, parent.width - 158)
            btnwidth: 50
            btnheight: 25
            btnRadius: 0
            btnColor: "#FFCC00"
            gradientColor: "#CCA300"
            textColor: "#000000"
            text: qsTr("Clear")
            textSize: 15
        }
    }

    GridLayout {
        id: settingsGrid
        //        x: Math.max(205, ((parent.width - 200) / 2) + 100 - 400)
        x: Math.max(205, rectangle3.x + (rectangle3.width / 2) - 400)
        y: 100
        columns: 2
        width: 800
        height: 270
        Rectangle {
            id: textBox
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: hostIpTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: hostIpTxt
                    anchors.centerIn: parent
                    text: qsTr("Host IP")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            TextField {
                id: textEdit1
                width: 190
                height: 38
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: TextInput.AlignHCenter
                selectByMouse: true
                inputMask: "000.000.000.000"
                text: qsTr("Enter desired host IP")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit1.width
                        height: textEdit1.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }
        Rectangle {
            id: textBox12
            width: 380
            height: 40
            color: "#b8cbde"
            visible: (settingsTab.selected == true)

            Rectangle {
                id: netNameTxtBox
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: netNameTxt
                    anchors.centerIn: parent
                    text: qsTr("Network Name")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            ComboBox {
                id: netNameCBox
                width: 189
                height: 38
                anchors.left: netNameTxtBox.right
                anchors.verticalCenter: netNameTxtBox.verticalCenter
                model: networkNameList //["Local Area Connection", "Local Area Connection 2", "Local Area Connection 3", "Local Area Connection 4"]
                style: ComboBoxStyle {
                    background: Rectangle {
                        color: enableSaveSetting? "#ffffff" : "#b8cbde"
                        Image {
                            anchors.right: parent.right
                            anchors.rightMargin: -2
                            width: 20
                            height: 20
                            anchors.verticalCenter: parent.verticalCenter
                            source: "arrow_down.png"
                        }
                    }
                    label: Text {
                        width: 190
                        height: 38
                        text: qsTr(control.currentText)
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        font.pixelSize: 16
                    }
                }
            }
            Rectangle {
                id: dummyRect
                width: enableSaveSetting? 0: 189
                height: 38
                opacity: 0
                anchors.left: netNameTxtBox.right
                anchors.verticalCenter: netNameTxtBox.verticalCenter
                MouseArea {
                    anchors.fill: parent
                }
            }
        }

        Rectangle {
            id: textBox6
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: tgtIpTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: tgtIpTxt
                    anchors.centerIn: parent
                    text: qsTr("Target IP")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            TextField {
                id: textEdit7
                width: 190
                height: 38
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: TextInput.AlignHCenter
                selectByMouse: true
                inputMask: "000.000.000.000"
                text: qsTr("137.202.77.105")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit1.width
                        height: textEdit1.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox1
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: tcpPortTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: tcpPortTxt
                    anchors.centerIn: parent
                    text: qsTr("Host TCP Port")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            TextField {
                id: textEdit2
                width: 190
                height: 38
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: TextInput.AlignHCenter
                validator: IntValidator {bottom: 0; top: 65535;}
                selectByMouse: true
                text: qsTr("Enter Port Number")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit2.width
                        height: textEdit2.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox8
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: tgtPortTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: tgtPortTxt
                    anchors.centerIn: parent
                    text: qsTr("DoIP Port")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }

            TextField {
                id: textEdit9
                width: 190
                height: 38
                anchors.right: parent.right
                horizontalAlignment: TextInput.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                validator: IntValidator {bottom: 1; top: 65535;}
                selectByMouse: true
                text: qsTr("13400")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit9.width
                        height: textEdit9.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox2
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: udpPortTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: udpPortTxt
                    anchors.centerIn: parent
                    text: qsTr("Host UDP Port")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            TextField {
                id: textEdit3
                width: 190
                height: 38
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: TextInput.AlignHCenter
                validator: IntValidator {bottom: 0; top: 65535;}
                selectByMouse: true
                text: qsTr("Enter Port Number")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit3.width
                        height: textEdit3.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox5
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: testerAddrTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: testerAddrTxt
                    anchors.centerIn: parent
                    text: qsTr("Tester Address")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }

            TextField {
                id: textEdit6
                width: 190
                height: 38
                anchors.right: parent.right
                horizontalAlignment: TextInput.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                inputMask: "\\0\\xHh \\0\\xHh"
                selectByMouse: true
                text: qsTr("0x00 0x65")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit6.width
                        height: textEdit6.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox3
            visible: settingsTab.selected == true
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: widthTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: widthTxt
                    anchors.centerIn: parent
                    text: qsTr("Application Width")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            TextField {
                id: textEdit4
                width: 190
                height: 38
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                horizontalAlignment: TextInput.AlignHCenter
                validator: IntValidator {bottom: 600; top: 1920;}
                selectByMouse: true
                text: qsTr("1280")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit4.width
                        height: textEdit4.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox7
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: doipNodeAddrTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: doipNodeAddrTxt
                    anchors.centerIn: parent
                    text: qsTr("DoIP Node Address")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }

            TextField {
                id: textEdit8
                width: 190
                height: 38
                anchors.right: parent.right
                horizontalAlignment: TextInput.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                inputMask: "\\0\\xHh \\0\\xHh"
                selectByMouse: true
                text: qsTr("0x00 0xC8")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit8.width
                        height: textEdit8.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox4
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: heightTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: heightTxt
                    anchors.centerIn: parent
                    text: qsTr("Application Height")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            TextField {
                id: textEdit5
                width: 190
                height: 38
                anchors.right: parent.right
                horizontalAlignment: TextInput.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                validator: IntValidator {bottom: 200; top: 2000;}
                selectByMouse: true
                text: qsTr("800")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit5.width
                        height: textEdit5.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox10
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: shPortTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: shPortTxt
                    anchors.centerIn: parent
                    text: qsTr("Seat Heater Mode Port")
                    color: "#ffffff"
                    font.pointSize: 10
                }
            }
            TextField {
                id: textEdit10
                width: 190
                height: 38
                anchors.right: parent.right
                horizontalAlignment: TextInput.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                validator: IntValidator {bottom: 1; top: 65535;}
                selectByMouse: true
                text: qsTr("12345")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit10.width
                        height: textEdit10.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }

        Rectangle {
            id: textBox11
            visible: (settingsTab.selected == true)
            width: 380
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: shTempPortTxtBox
                visible: settingsTab.selected == true
                width: 190
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: shTempPortTxt
                    anchors.centerIn: parent
                    text: qsTr("Seat Heater Temp. Port")
                    color: "#ffffff"
                    font.pointSize: 10
                }
            }
            TextField {
                id: textEdit11
                width: 190
                height: 38
                anchors.right: parent.right
                horizontalAlignment: TextInput.AlignHCenter
                anchors.verticalCenter: parent.verticalCenter
                validator: IntValidator {bottom: 1; top: 65535;}
                selectByMouse: true
                text: qsTr("12345")
                readOnly: enableSaveSetting? false : true
                font.pointSize: 12
                style: TextFieldStyle {
                    background: Rectangle {
                        width: textEdit10.width
                        height: textEdit10.height
                        color: control.readOnly? "#b8cbde" : "#ffffff"
                    }
                }
            }
        }
    }
    CheckBox {
        id: ipResetChkBox
        anchors.horizontalCenter: settingsGrid.horizontalCenter
        anchors.top: settingsGrid.bottom
        anchors.topMargin: 20
        visible: settingsTab.selected == true
        style: CheckBoxStyle {
            indicator: Rectangle {
                implicitWidth: 16
                implicitHeight: 16
                radius: 3
                border.color: control.activeFocus ? "darkblue" : "gray"
                border.width: 1
                Rectangle {
                    visible: control.checked
                    color: "#555"
                    border.color: "#333"
                    radius: 1
                    anchors.margins: 4
                    anchors.fill: parent
                }
            }
            label: Text {
                width: 250
                height: 50
                text: qsTr("Revert host IP on exit")
                color: "#b8cbde"
                font.pixelSize: 22
            }
        }
    }

    MyButton {
        property bool saveMode: false
        id: updateSettingBtn
        anchors.horizontalCenter: settingsGrid.horizontalCenter
        anchors.top: ipResetChkBox.bottom
        anchors.topMargin: 20
        visible: (settingsTab.selected == true)
        btnwidth: 120
        btnheight: 40
        btnRadius: 5
        btnColor: "#FFCC00"
        gradientColor: "#CCA300"
        textColor: "#000000"
        textSize: 26
        text: saveMode? qsTr("Save") : qsTr("Edit")
    }

    Rectangle {
        id: rectangle4
        x: 0
        y: 0
        width: parent.width
        height: 80

        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#0f4c7d"
            }

            GradientStop {
                position: 1
                color: "#000000"
            }
        }

        Image {
            id: btnIcon
            anchors.verticalCenter: parent.verticalCenter
            x: 25
            source: "logo-white.png"
            width: 311
            height: 35
        }

        Text {
            id: text3
            x: Math.max(350, (rectangle4.width / 2) - 160)
            anchors.verticalCenter: rectangle4.verticalCenter
            width: 320
            height: 32
            color: "#ffffff"
            text: qsTr("DoIP/Automotive Ethernet")
            style: Text.Normal
            font.family: "Arial"
            font.bold: true
            font.pixelSize: 30
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log(text3.width)
                    console.log(text3.height)
                }
            }
        }

        Rectangle {
            id: connectStatusRect
            x: Math.max(740, rectangle4.width - 125)
            anchors.verticalCenter: rectangle4.verticalCenter
            width: 100
            height: 30
            radius: 5
            color: connectionStatus? "#009933" : "#FF3300"

            Text {
                anchors.centerIn: parent
                text: connectionStatus? qsTr("Connected") : qsTr("Disconnected")
                font.pixelSize: 16
            }
        }
    }

    Rectangle {
        id: rectangle5
        x: 200
        anchors.top: rectangle4.bottom
        width: 1
        height: 720
        color: "#000000"
    }

    MyButton {
        id: homeTab
        x: 0
        y: 80
        isTab: true
        selected: true
        text: qsTr("Connect")
        //        icnSource: "home-icon.png"
    }

    MyButton {
        id: settingsTab
        x: 0
        y: 130
        isTab: true
        text: qsTr("Settings")
    }

    MyButton {
        id: diagTab
        x: 0
        y: 180
        isTab: true
        text: qsTr("Diagnostics")
    }

    MyButton {
        id: seatHeaterTab
        x: 0
        y: 230
        isTab: true
        text: qsTr("Seat Heater")
    }


    MyButton {
        id: helpTab
        x: 0
        y: 280
        isTab: true
        text: qsTr("Help")
    }


    MyButton {
        id: abtTab
        x: 0
        y: 330
        isTab: true
        text: qsTr("About")
    }

    MyButton {
        id: quitButton
        x: 0
        y: 380
        isTab: true
        text: qsTr("Exit")
    }

    Item {
        id: statusItem
        y: 120
        x: Math.max(205, ((parent.width - 200) / 2) + 200 - 190)
        visible: (homeTab.selected == true)
        width: 380
        height: 40
        Text {
            id: statusLbl
            width: 230
            height: 36
            anchors.verticalCenter: statusRect.verticalCenter
            text: qsTr("Connection status : ")
            color: "#b8cbde"
            font.pixelSize: 26
        }
        Rectangle {
            id: statusRect
            anchors.left: statusLbl.right
            anchors.leftMargin: 5
            width: 150
            height: 40
            radius: 5
            color: connectionStatus? "#009933" : "#FF3300"

            Text {
                anchors.centerIn: parent
                text: connectionStatus? qsTr("Connected") : qsTr("Disconnected")
                font.pixelSize: 22
            }
        }
    }
    CheckBox {
        id: connectTypeChkBox
        anchors.left: statusItem.left
        anchors.leftMargin: 10
        anchors.top: statusItem.bottom
        anchors.topMargin: 30
        visible: statusItem.visible
        style: CheckBoxStyle {
            indicator: Rectangle {
                implicitWidth: 16
                implicitHeight: 16
                radius: 3
                border.color: control.activeFocus ? "darkblue" : "gray"
                border.width: 1
                Rectangle {
                    visible: control.checked
                    color: "#555"
                    border.color: "#333"
                    radius: 1
                    anchors.margins: 4
                    anchors.fill: parent
                }
            }
            label: Text {
                width: 200
                height: 50
                text: qsTr("Connect automatically")
                color: "#b8cbde"
                font.pixelSize: 22
            }
        }
    }
    MyButton {
        id: connectBtn
        visible: statusItem.visible
        anchors.top: connectTypeChkBox.bottom
        anchors.topMargin: 30
        anchors.horizontalCenter: connectTypeChkBox.horizontalCenter
        btnwidth: 150
        btnheight: 40
        btnRadius: 5
        btnColor: "#FFCC00"
        gradientColor: "#CCA300"
        textColor: "#000000"
        text: connectionStatus? qsTr("Disconnect") : qsTr("Connect")
        textSize: 26
    }

    Item {
        id: seatHeaterItem
        y: 200
        visible: (seatHeaterTab.selected == true)
        x: Math.max(205, ((parent.width - 200) / 2) + 200 - 200)
        width: 400
        height: 40
        Rectangle {
            id: textBox9
            width: 352
            height: 40
            color: "#b8cbde"

            Rectangle {
                id: seatHeaterLblTxtBox
                width: 240
                height: 38
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.verticalCenter: parent.verticalCenter
                color: "#7D0032"

                Text {
                    id: seatHeaterLblTxt
                    anchors.centerIn: parent
                    text: qsTr("Set Seat Heater state")
                    color: "#ffffff"
                    font.pointSize: 12
                }
            }
            ComboBox {
                id: seatHeaterCBox
                anchors.left: seatHeaterLblTxtBox.right
                width: 110
                height: 38
                model: ["Off", "Low", "Medium", "High"]
                style: ComboBoxStyle {
                    background: Rectangle {
                        color: "#b8cbde"
                        Image {
                            anchors.right: parent.right
                            width: 20
                            height: 20
                            anchors.verticalCenter: parent.verticalCenter
                            source: "arrow_down.png"
                        }
                    }
                    label: Text {
                        width: 200
                        height: 50
                        text: qsTr(control.currentText)
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 22
                    }
                }
            }
        }
        //        Text {
        //            id: seatHeaterLbl
        //            width: 300
        //            height: 36
        //            anchors.verticalCenter: seatHeaterCBox.verticalCenter
        //            text: qsTr("Select seat heater state : ")
        //            color: "#b8cbde"
        //            font.pixelSize: 26
        //        }
    }
    MyButton {
        id: setSeatHeaterBtn
        visible: seatHeaterItem.visible
        anchors.top: seatHeaterItem.bottom
        anchors.topMargin: 30
        anchors.horizontalCenter: seatHeaterItem.horizontalCenter
        btnwidth: 120
        btnheight: 40
        btnRadius: 5
        btnColor: "#FFCC00"
        gradientColor: "#CCA300"
        textColor: "#000000"
        text: qsTr("Send")
        textSize: 26
    }

    ScrollView {
        x: Math.max(205, ((parent.width - 200) / 2) + 200 - 350)
        y: 106
        width: 800
        height: 350
        visible: helpTab.selected == true
        horizontalScrollBarPolicy: Qt.ScrollBarAsNeeded
        verticalScrollBarPolicy: Qt.ScrollBarAsNeeded
        Text {
            id: helpText
            width: 750
            //text: qsTr("Help - Diagnostics over Internet Protocol/Automotive Ethernet\n\nConnect:\n - DoIP tool will be in connected state after validating VID and Routing Activation Requests.\n - User can disconnect DoIP tool in the connections screen.\n - DoIP will be connected automatically as soon as DoIP tool is launched if “connect automatically” check box is enabled.\n - Tool should be in connected state to send any Diagnostic message to EVB.\n - Check ping status between host and target if tool is not able to connect to EVB.\n\nSettings:\n - Tester Address, DoIP Node Address : 2 byte hex value\n - Select Rever Host IP check box to restore the host's IP to the value on DoIP tool launch at DoIP tool exit\n\nDiagnostics:\n - WriteDID_Request : 2 byte hex value")
            font.family: "Arial"
            wrapMode: Text.WordWrap
            font.pixelSize: 20
            color: "#F9FCFF"
        }
    }

    Text {
        id: abtText
        x: Math.max(205, ((parent.width - 200) / 2) + 200 - 350)
        y: 106
        width: 750
        height: 300
        visible: abtTab.selected == true
        text: qsTr("Diagnostics over Internet Protocol/Automotive Ethernet Version 1.0.0\n\nMentor Automotive(R)\nhttp://www.mentor.com/automotive\n\nThis tool is used to get diagnostics information over Internet Protocol as well as to demonstrate Automotive Ethernet features.\n\nCopyright 2015 Mentor Graphics Inc. All rights reserved.")
        font.family: "Arial"
        wrapMode: Text.WordWrap
        elide: Text.ElideRight
        font.pixelSize: 20
        color: "#F9FCFF"
    }

    Connections {
        target: socintf

        onUpdateStatus: {
            printTrace(text);
        }
        onConnectionStatusChanged: {
            connectionStatus = status;
            printTrace("EVB connection status : " + status);
        }

        onNetworkNameListChanged: {
            networkNameList = sList;
        }

        onDataReceived: {
            if("DiagAck" == status["Name"]) {
                diagAckPV.text = "";
                diagAckIPV.text = "";
                diagAckType.text = "";
                diagAckLen.text = "";
                diagAckData.text = "";
                diagAckStatusRect.color = status["Status"] == 2? "#009933" : "#FF3300"
                diagAckStatus.text = status["Status"] == 2? "Success" : "Failure"
                diagAckPV.text = status["PV"];
                diagAckIPV.text = status["IPV"];
                diagAckType.text = status["Type"];
                diagAckLen.text = status["Length"];
                diagAckData.text = status["Data"];
            }
            if("Request" == status["Name"]) {
                reqPV.text = "";
                reqIPV.text = "";
                reqType.text = "";
                reqLen.text = "";
                reqData.text = "";
                reqStatusRect.color = status["Status"] == 2? "#009933" : "#FF3300"
                reqStatus.text = status["Status"] == 2? "Success" : "Failure"
                reqPV.text = status["PV"];
                reqIPV.text = status["IPV"];
                reqType.text = status["Type"];
                reqLen.text = status["Length"];
                reqData.text = status["Data"];
            }
            if("Response" == status["Name"]) {
                responsePV.text = "";
                responseIPV.text = "";
                responseType.text = "";
                responseLen.text = "";
                responseData.text = "";
                responseStatusRect.color = status["Status"] == 2? "#009933" : "#FF3300"
                responseStatus.text = status["Status"] == 2? "Success" : "Failure"
                responsePV.text = status["PV"];
                responseIPV.text = status["IPV"];
                responseType.text = status["Type"];
                responseLen.text = status["Length"];
                responseData.text = status["Data"];
            }
        }
    }

    Connections {
        target: settingintf
        onDiagSettingsChanged: {
            //            var setting = settingintf.diagSettings
            //            newIP.text = setting["hostIP"];
            //            udpPortNum.text = setting["udpPort"];
            //            tcpPortNum.text = setting["tcpPort"];
            //            appWidth.text = setting["width"];
            //            appHeight.text = setting["height"];
            //            tgtIP.text = setting["tgtIP"];
            //            tgtPortNum.text = setting["tgtPort"];
            //            testerAddress = setting["testerAddr"];
            //            doipNodeAddress = setting["doipNodeAddr"];
//            seatHeaterModePort = setting["seatHeaterModePort"];
//            seatHeaterTempPort = setting["seatHeaterTempPort"];

            socintf.connectToTarget();
            //            printTrace(retval);
        }

        onConnectTypeChanged: {
            connectType.checked = settingintf.connectType;
        }
        onResetIPChanged: {
            resetIpOnExit.checked = settingintf.resetIP;
        }
    }

}

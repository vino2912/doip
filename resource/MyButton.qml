import QtQuick 2.0

Item {
    id: myButton
    width: btnwidth
    height: btnheight

    property bool enabled: true
    property int hitboxExtension: 5
    property bool isTab: false
    property bool selected: false
    property alias btnRadius: btnRect.radius
    property alias text: label.text
    property alias textColor: label.color
    property alias gradient: btnRect.gradient
    property alias textSize: label.font.pixelSize
    property alias icnSource: btnIcon.source
    property string btnColor: "#0f4c7d"
    property int btnwidth: 200
    property int btnheight: 50
    property string gradientColor: "#000000"

    signal clicked

    Rectangle {
        id: btnRect
        width: parent.width
        height: parent.height

        MouseArea {
            id: hitbox
            anchors.fill: parent
            anchors.topMargin: -hitboxExtension
            anchors.bottomMargin: -hitboxExtension
            anchors.leftMargin: -hitboxExtension
            anchors.rightMargin: -hitboxExtension

            onPressed: {
                if(!myButton.enabled)
                    return;
                label.font.pixelSize = textSize - 2
                btnIcon.width = btnIcon.width - 8
                btnIcon.height = btnIcon.height - 8
            }
            onReleased: {
                if(!myButton.enabled)
                    return;
                label.font.pixelSize = textSize + 2
                btnIcon.width = btnIcon.width + 8
                btnIcon.height = btnIcon.height + 8
                if(true == isTab) {
                    myButton.selected = true
                }
                myButton.clicked()
            }
        }
        gradient: Gradient {
            GradientStop {
                position: 0
                color: {
                    if(selected == true)
                        "#4DB8FF"
                    else
                        btnColor
                }
            }

            GradientStop {
                position: 1
                color: {
                    if(selected == true)
                        "#66CCFF"
                    else
                        gradientColor
                }
            }
        }

        Text {
            id: label
//            anchors.centerIn: parent
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenterOffset: -1
            font.family: "Lato Black"
            font.pixelSize: 26
            color: "#ffffff"
            styleColor: "#4f000000"
            style: Text.Sunken
        }
        Image {
            id: btnIcon
            anchors.centerIn: parent
            width: 80
            height: 80
        }
    }
}

﻿Help - Diagnostics over Internet Protocol/Automotive Ethernet

Connect:
 - This tool changes to connected state after validating VID and Routing Activation Requests.
 - User can disconnect this tool any time in the connect screen.
 - This tool connects to EVB automatically as soon as it is launched if “connect automatically” check box is enabled.
 - Tool must be in connected state to send any Diagnostic message to EVB.
 - Verify ping status between host and EVB if tool is not able to connect with EVB.

Settings:
 - User is allowed to edit the desired values in this screen when the tool is in disconnected state.
 - Select "Revert Host IP check box" to restore the host's IP address when the tool is closed. Note that this will only restore the IP address which was set during tool launch.

Seat Heater:
 - This tool sends two UDP packets to EVB (one each to port 64100 and 64200 for Seat Heater Mode and Seat Heater Temperature values respectively) for each seat heater settings.

Diagnostics:
 - User is allowed to select the desired diagnostic message and send.
 - User has to enter 2 byte hex value when WriteDID_Request is selected.

#include <inc/worker.h>
#include "inc/ResponseMessages.h"
#include "inc/RequestMessages.h"

Worker::Worker(QObject *parent):
    QObject(parent), m_aWriteDIDVal{0x00, 0x10}, m_aTesterAddr{0x00, 0x65}, m_aDoIPNodeAddr{0x00, 0xC8}
{
    udpPortNum = 64404;
    tcpPortNum = 64403;
    tgtPortNum = 13400;
    seatHeaterModePortNum = 64100;
    seatHeaterTempPortNum = 64200;
    udpsocket = new QUdpSocket();
    tcpsocket = new QTcpSocket();

    sList << "Error!! Received data size doesn't match the expected size!";
    sList << "Error!! Received data doesn't match the expected data!";
    sList << "Correct Response!";
}

Worker::~Worker()
{
    disconnectFromTarget();
}

void Worker::bytesWritten(qint64 bytes)
{
    qDebug() << bytes << " bytes written...";
}

void Worker::udpReadData()
{
    qDebug() << "udp data";
    int RxBytes;
    char ResponseMsg[100];
    int retVal = 0;
    bool bSendData = true;
    QVariantMap map;

    char VID_Response[] ={0x02, 0xFD, 0x00, 0x04, 0x00, 0x00, 0x00, 0x21, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                          0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    while(udpsocket->hasPendingDatagrams()) {
        RxBytes = udpsocket->readDatagram((char *) ResponseMsg, (qint64) 100);
        //        printFrame(ResponseMsg,RxBytes);
    }
    if((0x00 == (unsigned char)ResponseMsg[3]) && (0x00 == (unsigned char)ResponseMsg[2]) && (0x00 == (unsigned char)ResponseMsg[8]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, Generic_negativeAck_wrongPrtocolVersion, 9);
        emit updateStatus("Wrong Protocol version : " + sList[retVal]);
    }
    else if((0x00 == (unsigned char)ResponseMsg[3]) && (0x00 == (unsigned char)ResponseMsg[2]) && (0x01 == (unsigned char)ResponseMsg[8]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, Generic_negativeAck_notSupportedPayloadType, 9);
        emit updateStatus("Not Supported payload type : " + sList[retVal]);
    }
    else if((0x00 == (unsigned char)ResponseMsg[3]) && (0x00 == (unsigned char)ResponseMsg[2]) && (0x02 == (unsigned char)ResponseMsg[8]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, Generic_negativeAck_longPayload, 9);
        emit updateStatus("Long payload : " + sList[retVal]);
    }
    else if((0x00 == (unsigned char)ResponseMsg[3]) && (0x00 == (unsigned char)ResponseMsg[2]) && (0x04 == (unsigned char)ResponseMsg[8]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, Generic_negativeAck_payloadLengthMismatch, 9);
        emit updateStatus("Payload length mismatch: " + sList[retVal]);
    }
    else if((0x02 == (unsigned char)ResponseMsg[3]) && (0x40 == (unsigned char)ResponseMsg[2]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, DiagEntityStatus_Response, 15);
        emit updateStatus("Entity Status : " + sList[retVal]);
    }
    else if((0x04 == (unsigned char)ResponseMsg[3]) && (0x40 == (unsigned char)ResponseMsg[2]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, DiagPwMdInfo_Response, 15);
        emit updateStatus("Diagnostic Power Mode Information : " + sList[retVal]);
    }
    else
    {
        retVal =  AssertResponse(ResponseMsg, VID_Response, 41);
        emit updateStatus("VID Response : " + sList[retVal]);
        if (2 == retVal)
        {
            sendRequest("Routing_Activation_Request");
        }
        bSendData = false;
    }

    if((0 != RxBytes) && (true == bSendData)) {
        map["Status"] = retVal;
        map["PV"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[0]);
        map["IPV"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[1]);
        map["Type"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[2]) + QString().sprintf("%02X", (unsigned char)ResponseMsg[3]);
        map["Length"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[4])
                + QString().sprintf("%02X", (unsigned char)ResponseMsg[5]) + QString().sprintf("%02X", (unsigned char)ResponseMsg[6])
                + QString().sprintf("%02X", (unsigned char)ResponseMsg[7]);
        map["Data"] = "";

        int len = (unsigned char)ResponseMsg[7];
        qDebug() << "loop " << len;
        for(int loop = 8; len > 0; len--, loop++)
        {
            map["Data"] = map["Data"].toString() + QString().sprintf("0x%02X, ", (unsigned char)ResponseMsg[loop]);
        }
        emit dataReceived(QVariant(map));
    }
}

void Worker::socketConnected()
{
    qDebug() << "socket connected";
    qDebug() << "tcp socket details : " << tcpsocket->localAddress() << tcpsocket->localPort();
    emit updateStatus("Connect to target " + target.toString() + " successful");
    while(tcpsocket->canReadLine())
    {
        QByteArray ba = tcpsocket->readLine();
        qDebug("from server: %s", ba.constData());
    }
    sendRequest("VID_Request");
}

void Worker::socketDisconnected()
{
    qDebug() << "socket disconnected";
    emit updateStatus("Socket " + QString::number(tgtPortNum) + " disconnected");
    m_ConnectionStatus = false;
    emit connectionStatusChanged(m_ConnectionStatus);
}

void Worker::tcpReadData()
{
    qDebug() << "tcpReadData read";
    int RxBytes = -1;
    char ResponseMsg[100];
    int retVal = -1;
    QVariantMap map;

    /*Expected response messages*/
    char RA_Response[] ={0x02, 0xFD, 0x00, 0x06, 0x00, 0x00, 0x00, 0x0D, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65,0x00, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    //    char Diag_Ack [] = {0x02, 0xFD, 0x80, 0x02, 0x00, 0x00, 0x00, 0x06, 0x00, 0xC8, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00};
    //    char CurrentDiagSessionDefault_Response[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x08, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x62, 0xF1, 0x86, 0x01};
    //    char CurrentDiagSessionExtended_Response[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x08, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x62, 0xF1, 0x86, 0x03};
    //    char RequestExtendedSes_Response[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x0A, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x50, 0x01, 0x00, 0xC8, 0x01, 0xF4};
    //    char ReadDID_Response_0[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x08, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x62, 0x00, 0x00, 0x00};
    //    char ReadDID_Response_10[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x08, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x62, 0x00, 0x00, 0x10};
    //    char WriteDID_Response[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x07, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x6E, 0x00, 0x00};

    RxBytes = tcpsocket->read((char *) ResponseMsg, (qint64) 100);

    if((0x06 == (unsigned int)ResponseMsg[3]) && (0x00 == (unsigned int)ResponseMsg[2]))
    {
        retVal = AssertResponse(ResponseMsg, RA_Response, 21);
        emit updateStatus("Routing Activation : " + sList[retVal]);

        if(2 == retVal) {
            m_ConnectionStatus = true;
            emit connectionStatusChanged(m_ConnectionStatus);
        }
    }
    else if((0x02 == (unsigned char)ResponseMsg[3]) && (0x80 == (unsigned char)ResponseMsg[2]))
    {
        map["Name"] = "DiagAck";
        retVal = AssertResponse(ResponseMsg, Diag_Ack, 13);
        emit updateStatus("Diag Ack : " + sList[retVal]);
    }
    else if((0x01 == (unsigned char)ResponseMsg[3]) && (0x80 == (unsigned char)ResponseMsg[2]) && (0x62 == (unsigned char)ResponseMsg[12])
            && (0xF1 == (unsigned char)ResponseMsg[13]) && (0x86 == (unsigned char)ResponseMsg[14]) && (0x01 == (unsigned char)ResponseMsg[15]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, CurrentDiagSessionDefault_Response, 16);
        emit updateStatus("Current Diagnostic Session Default : " + sList[retVal]);
    }
    else if((0x01 == (unsigned char)ResponseMsg[3]) && (0x80 == (unsigned char)ResponseMsg[2]) && (0x62 == (unsigned char)ResponseMsg[12])
            && (0xF1 == (unsigned char)ResponseMsg[13]) && (0x86 == (unsigned char)ResponseMsg[14]) && (0x03 == (unsigned char)ResponseMsg[15]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, CurrentDiagSessionExtended_Response, 16);
        emit updateStatus("Current Diagnostic Session Extended : " + sList[retVal]);
    }
    else if((0x01 == (unsigned char)ResponseMsg[3]) && (0x80 == (unsigned char)ResponseMsg[2]) && (0x50 == (unsigned char)ResponseMsg[12]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, RequestExtendedSes_Response, 18);
        emit updateStatus("Request Extended Session : " + sList[retVal]);
    }
    else if((0x01 == (unsigned char)ResponseMsg[3]) && (0x80 == (unsigned char)ResponseMsg[2]) && (0x62 == (unsigned char)ResponseMsg[12]))
    {
        map["Name"] = "Response";
        ReadDID_Response_0[15] = m_aWriteDIDVal[1];
        retVal = AssertResponse(ResponseMsg, ReadDID_Response_0, 16);
        emit updateStatus("Read DID : " + sList[retVal]);
    }
    else if((0x01 == (unsigned char)ResponseMsg[3]) && (0x80 == (unsigned char)ResponseMsg[2]) && (0x6E == (unsigned char)ResponseMsg[12]))
    {
        map["Name"] = "Response";
        retVal = AssertResponse(ResponseMsg, WriteDID_Response, 15);
        emit updateStatus("Write DID : " + sList[retVal]);
    }

    if(0 != RxBytes) {
        map["Status"] = retVal;
        map["PV"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[0]);
        map["IPV"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[1]);
        map["Type"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[2]) + QString().sprintf("%02X", (unsigned char)ResponseMsg[3]);
        map["Length"] = QString().sprintf("0x%02X", (unsigned char)ResponseMsg[4])
                + QString().sprintf("%02X", (unsigned char)ResponseMsg[5]) + QString().sprintf("%02X", (unsigned char)ResponseMsg[6])
                + QString().sprintf("%02X", (unsigned char)ResponseMsg[7]);
        map["Data"] = "";

        int len = (unsigned char)ResponseMsg[7];
        qDebug() << "loop " << len;
        for(int loop = 8; len > 0; len--, loop++)
        {
            map["Data"] = map["Data"].toString() + QString().sprintf("0x%02X, ", (unsigned char)ResponseMsg[loop]);
        }
        emit dataReceived(QVariant(map));
    }
    //    printFrame(ResponseMsg,RxBytes);
}

void Worker::sendSeatHeaterValue(QString value)
{
    int retval = -1;
    int mode = 0;
    int temperature = 0;

    if("Off" == value)
    {
        mode = 10;
        temperature = 0;
    }
    else if("Low" == value)
    {
        mode = 1;
        temperature = 30;
    }
    else if("Medium" == value)
    {
        mode = 2;
        temperature = 35;
    }
    else if("High" == value)
    {
        mode = 5;
        temperature = 40;
    }

    retval = udpsocket->writeDatagram((char *) &mode, 1, target, seatHeaterModePortNum);
    if (-1 == retval) {
        emit updateStatus(QString("Sending Seat Heater Mode Value failed"));
        return;
    }

    retval = udpsocket->writeDatagram((char *) &temperature, 1, target, seatHeaterTempPortNum);
    if (-1 == retval) {
        emit updateStatus(QString("Sending Seat Heater Mode Value failed"));
        return;
    }
    emit updateStatus(QString("Seat Heater value sent as: " + value));
}

void Worker::sendRequest(QString request)
{
    int retval = -1;
    char *bytes = 0;

    if ((false == m_ConnectionStatus) && (("VID_Request" != request) && ("Routing_Activation_Request" != request))) {
        emit updateStatus("Target not connected");
        return;
    }

    /*Request messages */
    //    char VID_Req[] ={0x02, 0xFD, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00};
    //    char VIDwEID_Req[]={0x02, 0xFD, 0x00, 0x02, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x01, 0xF4};
    //    char VIDwVIN_Req[]={0x02, 0xFD, 0x00, 0x03, 0x00, 0x00, 0x00, 0x11, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11 };
    //    char RA_Req[] ={0x02, 0xFD, 0x00, 0x05, 0x00, 0x00, 0x00, 0x0B, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65,0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    //    char CurrentDiagSession_Req[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x07, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x22, 0xF1, 0x86};
    //    char RequestExtendedSes_Req[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x06, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x10, 0x03};
    //    char ReadDID_Req[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x07, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x22, 0x00, 0x00};
    //    char WriteDID_Req[] ={0x02, 0xFD, 0x80, 0x01, 0x00, 0x00, 0x00, 0x08, /*SrC Addr*/ 0x00,/*SrC Addr*/ 0x65, 0x00 ,0xC8, 0x2E, 0x00, 0x00,0x10};
    //    char DiagPwMdInfo_Req[]={0x02, 0xFD, 0x40, 0x03, 0x00, 0x00, 0x00, 0x00};
    //    char DiagEntityStatus_Req[]={0x02, 0xFD, 0x40, 0x01, 0x00, 0x00, 0x00, 0x00};

    if("VID_Request" == request)
        retval = udpsocket->writeDatagram((char *) VID_Req, 8, target, tgtPortNum);

    else if("VIDwWrongProtocolVersion_Req" == request) {
        retval = udpsocket->writeDatagram((char *) VIDwWrongProtocolVersion_Req, 8, target, tgtPortNum);
        bytes = VIDwWrongProtocolVersion_Req;
    }

    else if("LongPayload_Req" == request) {
        retval = udpsocket->writeDatagram((char *) LongPayload_Req, 72, target, tgtPortNum);
        bytes = LongPayload_Req;
    }

    else if("PayloadLengthMismatch_Req" == request) {
        retval = udpsocket->writeDatagram((char *) PayloadLengthMismatch_Req, 13, target, tgtPortNum);
        bytes = PayloadLengthMismatch_Req;
    }

    else if("InvalidPayloadType_Req" == request) {
        retval = udpsocket->writeDatagram((char *) InvalidPayloadType_Req, 8, target, tgtPortNum);
        bytes = InvalidPayloadType_Req;
    }

    else if("PowerModeInfo_Req" == request) {
        retval = udpsocket->writeDatagram((char *) DiagPwMdInfo_Req, 8, target, tgtPortNum);
        bytes = DiagPwMdInfo_Req;
    }

    else if("EntityStatus_Req" == request) {
        retval = udpsocket->writeDatagram((char *) DiagEntityStatus_Req, 8, target, tgtPortNum);
        bytes = DiagEntityStatus_Req;
    }

    else if("Routing_Activation_Request" == request) {
        RA_Req[8] = m_aTesterAddr[0];
        RA_Req[9] = m_aTesterAddr[1];
        qDebug() << "RA_REQ : " << RA_Req[8] << RA_Req[9];
        retval = tcpsocket->write((char *) RA_Req, 19);
    }

    else if("ReadDID_Req" == request) {
        ReadDID_Req[8] = m_aTesterAddr[0];
        ReadDID_Req[9] = m_aTesterAddr[1];
        ReadDID_Req[10] = m_aDoIPNodeAddr[0];
        ReadDID_Req[11] = m_aDoIPNodeAddr[1];
        retval = tcpsocket->write((char *) ReadDID_Req, 15);
        bytes = ReadDID_Req;
    }
    else if("WriteDID_Req" == request) {
        WriteDID_Req[8] = m_aTesterAddr[0];
        WriteDID_Req[9] = m_aTesterAddr[1];
        WriteDID_Req[10] = m_aDoIPNodeAddr[0];
        WriteDID_Req[11] = m_aDoIPNodeAddr[1];
        WriteDID_Req[14] = m_aWriteDIDVal[0];
        WriteDID_Req[15] = m_aWriteDIDVal[1];
        retval = tcpsocket->write((char *) WriteDID_Req, 16);
        bytes = WriteDID_Req;
    }
    else if("Current_DiagSession_Req" == request) {
        CurrentDiagSession_Req[8] = m_aTesterAddr[0];
        CurrentDiagSession_Req[9] = m_aTesterAddr[1];
        CurrentDiagSession_Req[10] = m_aDoIPNodeAddr[0];
        CurrentDiagSession_Req[11] = m_aDoIPNodeAddr[1];
        retval = tcpsocket->write((char *) CurrentDiagSession_Req, 15);
        bytes = CurrentDiagSession_Req;
    }
    else if("ExtendedSession_Req" == request) {
        RequestExtendedSes_Req[8] = m_aTesterAddr[0];
        RequestExtendedSes_Req[9] = m_aTesterAddr[1];
        RequestExtendedSes_Req[10] = m_aDoIPNodeAddr[0];
        RequestExtendedSes_Req[11] = m_aDoIPNodeAddr[1];
        retval = tcpsocket->write((char *) RequestExtendedSes_Req, 14);
        bytes = RequestExtendedSes_Req;
    }

    qDebug() << "write returned : " << retval << tcpsocket->peerAddress() << tcpsocket->peerPort();

    if (-1 == retval)
    {
        if(0 != bytes) {
            QVariantMap map;
            map["Status"] = 1; // Success = 2; Fail = 1
            map["Name"] = "Request";
            map["PV"] = QString().sprintf("0x%02X", (unsigned char)bytes[0]);
            map["IPV"] = QString().sprintf("0x%02X", (unsigned char)bytes[1]);
            map["Type"] = QString().sprintf("0x%02X", (unsigned char)bytes[2]) + QString().sprintf("%02X", (unsigned char)bytes[3]);
            map["Length"] = QString().sprintf("0x%02X", (unsigned char)bytes[4])
                    + QString().sprintf("%02X", (unsigned char)bytes[5]) + QString().sprintf("%02X", (unsigned char)bytes[6])
                    + QString().sprintf("%02X", (unsigned char)bytes[7]);
            map["Data"] = "";

            int len = (unsigned char)bytes[7];
            qDebug() << "loop " << len;
            for(int loop = 8; len > 0; len--, loop++)
            {
                map["Data"] = map["Data"].toString() + QString().sprintf("0x%02X, ", (unsigned char)bytes[loop]);
            }
            emit dataReceived(QVariant(map));
        }
        emit updateStatus(QString("Sending ") + request + QString(" to EVB failed"));
        return;
    }
    else
    {
        if(0 != bytes) {
            QVariantMap map;
            map["Status"] = 2; // Success = 2; Fail = 1
            map["Name"] = "Request";
            map["PV"] = QString().sprintf("0x%02X", (unsigned char)bytes[0]);
            map["IPV"] = QString().sprintf("0x%02X", (unsigned char)bytes[1]);
            map["Type"] = QString().sprintf("0x%02X", (unsigned char)bytes[2]) + QString().sprintf("%02X", (unsigned char)bytes[3]);
            map["Length"] = QString().sprintf("0x%02X", (unsigned char)bytes[4])
                    + QString().sprintf("%02X", (unsigned char)bytes[5]) + QString().sprintf("%02X", (unsigned char)bytes[6])
                    + QString().sprintf("%02X", (unsigned char)bytes[7]);
            map["Data"] = "";

            int len = (unsigned char)bytes[7];
            qDebug() << "loop " << len;
            for(int loop = 8; len > 0; len--, loop++)
            {
                map["Data"] = map["Data"].toString() + QString().sprintf("0x%02X, ", (unsigned char)bytes[loop]);
            }
            emit dataReceived(QVariant(map));
        }
        emit updateStatus(request + QString(" sent to EVB"));
        return;
    }
}

void Worker::printFrame(const void *buffer, int buffer_len)
{
    if(buffer_len < 8 )
    {
        qDebug("\nInvalid message length %d bytes ",buffer_len);
        return;
    }
    const quint8 *ptr = (const quint8*)buffer;

    qDebug("protocol version      : %02x\n",(unsigned int)ptr[0]);
    qDebug("Inv. protocol version : %02x\n",(unsigned int)ptr[1]);
    qDebug("Payload Type          : %02x%02x\n",(unsigned int)ptr[2],(unsigned int)ptr[3]);
    qDebug("Payload Length        : %02x%02x%02x%02x (%d bytes)\n\n",(unsigned int)ptr[4],(unsigned int)ptr[5],(unsigned int)ptr[6],(unsigned int)ptr[7],buffer_len-8);

    if(buffer_len > 8){
        int counter;
        qDebug("Payload Data:\n[ ");
        for(counter=8; counter<buffer_len; counter++)
            qDebug("%02x,",(unsigned int)ptr[counter]);
        qDebug("]\n");
    }else{
        qDebug("No payload data.\n");
    }
}

void Worker::connectToTarget()
{
    qDebug() << udpsocket << tcpsocket;
    disconnectFromTarget();

    udpsocket = new QUdpSocket();
    tcpsocket = new QTcpSocket();

    connect(udpsocket, SIGNAL(readyRead()), this, SLOT(udpReadData()));
    connect(tcpsocket, SIGNAL(connected()), this, SLOT(socketConnected()));
    connect(tcpsocket, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
    connect(tcpsocket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));
    connect(tcpsocket, SIGNAL(readyRead()), this, SLOT(tcpReadData()));

    int retval = udpsocket->bind(QHostAddress::Any, udpPortNum);
    qDebug() << "UDP port bind returned : " << retval;
    retval = tcpsocket->bind(QHostAddress::Any, tcpPortNum);
    qDebug() << "TCP port bind " << tcpPortNum << " returned : " << retval;

    target.setAddress(targetIP);
    qDebug() << "tcp socket details : " << tcpsocket->localAddress() << tcpsocket->localPort();
    tcpsocket->connectToHost(target, tgtPortNum, QIODevice::ReadWrite);
    if (false == tcpsocket->waitForConnected())
    {
        emit updateStatus("Connect to target " + target.toString() + ":" + QString::number(tgtPortNum) + " failed : " + tcpsocket->errorString());
    }
    else
    {
        emit updateStatus("Socket " + QString::number(tgtPortNum) + " connected successfully"); //On success socket connected slot is called and success status is updated there.
    }
}

void Worker::disconnectFromTarget()
{
    if(0 != udpsocket) {
        udpsocket->close();
        delete udpsocket;
        udpsocket = 0;
    }
    if(0 != tcpsocket) {
        qDebug() << "Removing TCP socket connection";
        if(QAbstractSocket::ConnectedState == tcpsocket->state()) {
            tcpsocket->disconnectFromHost();
        }
        tcpsocket->close();
        tcpsocket->abort();
        delete tcpsocket;
        tcpsocket = 0;
    }

    if(true == m_ConnectionStatus) {
        m_ConnectionStatus = false;
        emit connectionStatusChanged(m_ConnectionStatus);
    }
}

void Worker::setTgtIP(QString newIP)
{
    qDebug() << "Inside setTgtIP" << newIP;
    if(targetIP == newIP)
    {
        emit updateStatus("Target IP unchanged");
        return;
    }

    QHostAddress address(newIP);
    if (QAbstractSocket::IPv4Protocol == address.protocol())
    {
        qDebug("Valid IPv4 address.");
        targetIP = newIP;
    }
    else if (QAbstractSocket::IPv6Protocol == address.protocol())
    {
        qDebug("Valid IPv6 address.");
        targetIP = newIP;
    }
    else
    {
        qDebug("Unknown or invalid address.");
        emit updateStatus(QString("Unknown or invalid target address"));
    }
    emit updateStatus(QString("Target IP updated"));
}

void Worker::setTesterAddress(QString sTesterAddress)
{
    getHexBytesFromHexString(sTesterAddress, m_aTesterAddr, 2);
    emit updateStatus(QString("Tester Address updated"));
}

void Worker::setDoIPNodeAddress(QString sDoIPNodeAddr)
{
    getHexBytesFromHexString(sDoIPNodeAddr, m_aDoIPNodeAddr, 2);
    emit updateStatus(QString("DoIP Node Address updated"));
}

void Worker::setPortNum(quint16 portNum, quint8 type)
{
    if(0 == type)
    {
        qDebug() << "Updated UDP Port Number : " << portNum;
        udpPortNum = portNum;
        emit updateStatus(QString("Set UDP port number success: ") + QString::number(portNum));
    }
    else if(1 == type)
    {
        qDebug() << "Updated TCP Port Number : " << portNum;
        tcpPortNum = portNum;
        emit updateStatus(QString("Set TCP port number success: ") + QString::number(portNum));
    }
    else if(2 == type)
    {
        qDebug() << "Updated Target Port Number : " << portNum;
        tgtPortNum = portNum;
        emit updateStatus(QString("Set DoIP port number success: ") + QString::number(portNum));
    }
    else if(3 == type)
    {
        qDebug() << "Updated Seat Heater Mode Port Number : " << portNum;
        seatHeaterModePortNum = portNum;
        emit updateStatus(QString("Set Seat Heater Mode port number success: ") + QString::number(portNum));
    }
    else if(4 == type)
    {
        qDebug() << "Updated Seat Heater Temperature Port Number : " << portNum;
        seatHeaterTempPortNum = portNum;
        emit updateStatus(QString("Set Seat Heater Temperature port number success: ") + QString::number(portNum));
    }
    else
    {
        emit updateStatus(QString("Set port number failed"));
    }
}

int Worker::AssertResponse (char * received, char * Expected, int len)
{
    int counter;
    if (sizeof(received) != sizeof(Expected))
    {
        return 0; //"Error!! Received data size doesn't match the expected size!";
    }
    for(counter=0; counter < len; counter++){
        if(received[counter] != Expected[counter])
        {
            return 1; //"Error!! Received data doesn't match the expected data!";
        }
    }

    return 2; //"Correct response!!";

}

void Worker::setWriteDIDValue(QString str)
{
    getHexBytesFromHexString(str, m_aWriteDIDVal, 2);
}

bool Worker::getHexBytesFromHexString(QString str, quint8 *result, qint16 result_array_len)
{
    QStringList sL = str.split(' ');
    bool ok;
    qDebug() << sL[0].toUInt(&ok, 16) << sL.at(1).toUInt(&ok, 16) << sL.length() << str;

    if(result_array_len != sL.length())
        return false;

    qint16 index = 0;
    while(index < sL.length()) {
        result[index] = sL[index].toUInt(&ok, 16);
        if(!ok) {
            qDebug() << "conversion failed";
            return false;
        }
        index++;
    }
    return true;
}

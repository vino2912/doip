#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include "inc/socketinterface.h"
#include "inc/settings.h"
#include <QIcon>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/icons/icon.png"));

    /* Register qml types */
    qmlRegisterType<SocInterface> ("com.mgc.evb.socintf", 1, 0, "SocIntf");
    qmlRegisterType<DiagSettings> ("com.mgc.evb.diagsettings", 1, 0, "DiagSettings");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}

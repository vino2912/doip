#include "inc/settings.h"

DiagSettings::DiagSettings(QObject *parent):
    QObject(parent)
{
    m_xmlFile.setFileName("settings.xml");
    m_appHeight = 800;
    m_appWidth = 1280;
    m_connectType = 0;
    m_resetIP = 0;

    if(!m_xmlFile.exists())
    {
        if(!m_xmlFile.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            qDebug() << "Failed to open settings xml file";
            return;
        }

        QDomElement root = m_document.createElement("Settings");
        m_document.appendChild(root);

        //Add the default values
        m_hostIp = m_document.createElement("HostIp");
        m_hostIp.appendChild(m_document.createTextNode(QString("137.202.77.199")));
        root.appendChild(m_hostIp);

        m_udpPort = m_document.createElement("UdpPort");
        m_udpPort.appendChild(m_document.createTextNode(QString("64404")));
        root.appendChild(m_udpPort);

        m_tcpPort = m_document.createElement("TcpPort");
        m_tcpPort.appendChild(m_document.createTextNode(QString("64403")));
        root.appendChild(m_tcpPort);

        QDomElement appWidth = m_document.createElement("Width");
        appWidth.appendChild(m_document.createTextNode(QString("1280")));
        root.appendChild(appWidth);

        QDomElement appHeight = m_document.createElement("Height");
        appHeight.appendChild(m_document.createTextNode(QString("800")));
        root.appendChild(appHeight);

        QDomElement connectType = m_document.createElement("ConnectionMode");
        connectType.appendChild(m_document.createTextNode(QString::number(m_connectType)));
        root.appendChild(connectType);

        QDomElement reset = m_document.createElement("ResetIP");
        reset.appendChild(m_document.createTextNode(QString::number(m_resetIP)));
        root.appendChild(reset);

        QDomElement tgtIP = m_document.createElement("TargetIP");
        tgtIP.appendChild(m_document.createTextNode(QString("137.202.77.106")));
        root.appendChild(tgtIP);

        m_tgtPort = m_document.createElement("TargetPort");
        m_tgtPort.appendChild(m_document.createTextNode(QString("13400")));
        root.appendChild(m_tgtPort);

        m_testerAddr = m_document.createElement("TesterAddr");
        m_testerAddr.appendChild(m_document.createTextNode(QString("0x00 0x65")));
        root.appendChild(m_testerAddr);

        m_doipNodeAddr = m_document.createElement("DoIPNodeAddr");
        m_doipNodeAddr.appendChild(m_document.createTextNode(QString("0x00 0xC8")));
        root.appendChild(m_doipNodeAddr);

        m_shModePort = m_document.createElement("SeatHeaterModePort");
        m_shModePort.appendChild(m_document.createTextNode(QString("64100")));
        root.appendChild(m_shModePort);

        m_shTempPort = m_document.createElement("SeatHeaterTempPort");
        m_shTempPort.appendChild(m_document.createTextNode(QString("64200")));
        root.appendChild(m_shTempPort);

        m_networkName = m_document.createElement("NetworkConnectionName");
        m_networkName.appendChild(m_document.createTextNode(QString("Local Area Connection")));
        root.appendChild(m_networkName);

        //Write to file
        QTextStream stream(&m_xmlFile);
        stream << m_document.toString();
        //        m_xmlFile.close();
        qDebug() << "Created settings.xml file";

        QVariantMap settingsMap;
        settingsMap["hostIP"] = "137.202.77.199";
        settingsMap["udpPort"] = "64404";
        settingsMap["tcpPort"] = "64403";
        settingsMap["width"] = "1280";
        settingsMap["height"] = "800";
        settingsMap["tgtIP"] = "137.202.77.106";
        settingsMap["tgtPort"] = "13400";
        settingsMap["testerAddr"] = "0x00 0x65";
        settingsMap["doipNodeAddr"] = "0x00 0xC8";
        settingsMap["seatHeaterModePort"] = "64100";
        settingsMap["seatHeaterTempPort"] = "64200";
        settingsMap["networkConnection"] = "Local Area Connection";

        m_diagSettings = QVariant(settingsMap);
    }
    else{

        if(!m_xmlFile.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            qDebug() << "Failed to open settings xml file";
            return;
        }
        if(!m_document.setContent(&m_xmlFile))
        {
            qDebug() << "Failed to load document";
            m_xmlFile.close();
            return;
        }
        //get the root element
        QDomElement root = m_document.firstChildElement();
        m_hostIp = root.elementsByTagName(QString("HostIp")).at(0).toElement();
        qDebug() << "Host Ip : " << m_hostIp.text();

        m_udpPort = root.elementsByTagName(QString("UdpPort")).at(0).toElement();
        qDebug() << "Udp Port : " << m_udpPort.text();

        m_tcpPort = root.elementsByTagName(QString("TcpPort")).at(0).toElement();
        qDebug() << "Tcp Port : " << m_tcpPort.text();

        QDomElement width = root.elementsByTagName(QString("Width")).at(0).toElement();
        m_appWidth = width.text().toInt();
        qDebug() << "Application Width : " << width.text().toInt();

        QDomElement height = root.elementsByTagName(QString("Height")).at(0).toElement();
        m_appHeight = height.text().toInt();
        qDebug() << "Application Height : " << height.text().toInt();

        QDomElement connectType = root.elementsByTagName(QString("ConnectionMode")).at(0).toElement();
        m_connectType = connectType.text().toInt();

        QDomElement reset = root.elementsByTagName(QString("ResetIP")).at(0).toElement();
        m_resetIP = reset.text().toInt();

        m_tgtIP = root.elementsByTagName(QString("TargetIP")).at(0).toElement();
        qDebug() << "Target Ip : " << m_tgtIP.text();

        m_tgtPort = root.elementsByTagName(QString("TargetPort")).at(0).toElement();
        qDebug() << "Target Port : " << m_tgtPort.text();

        m_testerAddr = root.elementsByTagName(QString("TesterAddr")).at(0).toElement();
        qDebug() << "Tester Address : " << m_testerAddr.text();

        m_doipNodeAddr = root.elementsByTagName(QString("DoIPNodeAddr")).at(0).toElement();
        qDebug() << "DoIP Node Address : " << m_doipNodeAddr.text();

        m_shModePort = root.elementsByTagName(QString("SeatHeaterModePort")).at(0).toElement();
        qDebug() << "Seat Heater Mode Port : " << m_shModePort.text();

        m_shTempPort = root.elementsByTagName(QString("SeatHeaterTempPort")).at(0).toElement();
        qDebug() << "Seat Heater Temperature Port : " << m_shTempPort.text();

        m_networkName = root.elementsByTagName(QString("NetworkConnectionName")).at(0).toElement();
        qDebug() << "Network Connection Name : " << m_networkName.text();

        QVariantMap settingsMap;
        settingsMap["hostIP"] = m_hostIp.text();
        settingsMap["udpPort"] = m_udpPort.text();
        settingsMap["tcpPort"] = m_tcpPort.text();
        settingsMap["width"] = m_appWidth;
        settingsMap["height"] = m_appHeight;
        settingsMap["tgtIP"] = m_tgtIP.text();
        settingsMap["tgtPort"] = m_tgtPort.text();
        settingsMap["testerAddr"] = m_testerAddr.text();
        settingsMap["doipNodeAddr"] = m_doipNodeAddr.text();
        settingsMap["seatHeaterModePort"] = m_shModePort.text();
        settingsMap["seatHeaterTempPort"] = m_shTempPort.text();
        settingsMap["networkConnection"] = m_networkName.text();
        m_diagSettings = QVariant(settingsMap);

        emit diagSettingsChanged();
        emit connectTypeChanged();
        emit resetIPChanged();
    }
}

void DiagSettings::setDiagSettings(QVariant updatedSettings)
{
    qDebug() << updatedSettings;
    QVariantMap settingsMap = updatedSettings.toMap();
    m_diagSettings = updatedSettings;
    m_appWidth = settingsMap.value("width").toInt();
    m_appHeight = settingsMap.value("height").toInt();

    QDomElement root = m_document.firstChildElement();
    QDomElement presentIp = root.elementsByTagName(QString("HostIp")).at(0).toElement();
    QDomElement newHostIp = m_document.createElement("HostIp");
    newHostIp.appendChild(m_document.createTextNode(settingsMap.value("hostIP").toString()));
    root.replaceChild(newHostIp, presentIp);

    QDomElement presentUdpPort = root.elementsByTagName(QString("UdpPort")).at(0).toElement();
    QDomElement newUdpPort = m_document.createElement("UdpPort");
    newUdpPort.appendChild(m_document.createTextNode(settingsMap.value("udpPort").toString()));
    root.replaceChild(newUdpPort, presentUdpPort);

    QDomElement presentTcpPort = root.elementsByTagName(QString("TcpPort")).at(0).toElement();
    QDomElement newTcpPort = m_document.createElement("TcpPort");
    newTcpPort.appendChild(m_document.createTextNode(settingsMap.value("tcpPort").toString()));
    root.replaceChild(newTcpPort, presentTcpPort);

    QDomElement presentWidth = root.elementsByTagName(QString("Width")).at(0).toElement();
    QDomElement newWidth = m_document.createElement("Width");
    newWidth.appendChild(m_document.createTextNode(settingsMap.value("width").toString()));
    root.replaceChild(newWidth, presentWidth);

    QDomElement presentHeight = root.elementsByTagName(QString("Height")).at(0).toElement();
    QDomElement newHeight = m_document.createElement("Height");
    newHeight.appendChild(m_document.createTextNode(settingsMap.value("height").toString()));
    root.replaceChild(newHeight, presentHeight);

    QDomElement presenttgtIP = root.elementsByTagName(QString("TargetIP")).at(0).toElement();
    QDomElement newtgtIP = m_document.createElement("TargetIP");
    newtgtIP.appendChild(m_document.createTextNode(settingsMap.value("tgtIP").toString()));
    root.replaceChild(newtgtIP, presenttgtIP);

    QDomElement presenttgtPort = root.elementsByTagName(QString("TargetPort")).at(0).toElement();
    QDomElement newtgtPort = m_document.createElement("TargetPort");
    newtgtPort.appendChild(m_document.createTextNode(settingsMap.value("tgtPort").toString()));
    root.replaceChild(newtgtPort, presenttgtPort);

    QDomElement presentTesterAddr = root.elementsByTagName(QString("TesterAddr")).at(0).toElement();
    QDomElement newTesterAddr = m_document.createElement("TesterAddr");
    newTesterAddr.appendChild(m_document.createTextNode(settingsMap.value("testerAddr").toString()));
    root.replaceChild(newTesterAddr, presentTesterAddr);

    QDomElement presentDoIPNodeAddr = root.elementsByTagName(QString("DoIPNodeAddr")).at(0).toElement();
    QDomElement newDoIPNodeAddr = m_document.createElement("DoIPNodeAddr");
    newDoIPNodeAddr.appendChild(m_document.createTextNode(settingsMap.value("doipNodeAddr").toString()));
    root.replaceChild(newDoIPNodeAddr, presentDoIPNodeAddr);

    QDomElement presentModePort = root.elementsByTagName(QString("SeatHeaterModePort")).at(0).toElement();
    QDomElement newModePort = m_document.createElement("SeatHeaterModePort");
    newModePort.appendChild(m_document.createTextNode(settingsMap.value("seatHeaterModePort").toString()));
    root.replaceChild(newModePort, presentModePort);

    QDomElement presentTempPort = root.elementsByTagName(QString("SeatHeaterTempPort")).at(0).toElement();
    QDomElement newTempPort = m_document.createElement("SeatHeaterTempPort");
    newTempPort.appendChild(m_document.createTextNode(settingsMap.value("seatHeaterTempPort").toString()));
    root.replaceChild(newTempPort, presentTempPort);

    QDomElement presentNetConnection = root.elementsByTagName(QString("NetworkConnectionName")).at(0).toElement();
    QDomElement newNetConnection = m_document.createElement("NetworkConnectionName");
    newNetConnection.appendChild(m_document.createTextNode(settingsMap.value("networkConnection").toString()));
    root.replaceChild(newNetConnection, presentNetConnection);

    m_xmlFile.resize(0);
    QTextStream stream(&m_xmlFile);
    stream << m_document.toString();
    qDebug() << "Updated settings.xml file";

    emit appWidthChanged();
    emit appHeightChanged();
    emit diagSettingsChanged();
}

void DiagSettings::setConnectType(int connectType)
{
    m_connectType = connectType;
    QDomElement root = m_document.firstChildElement();
    QDomElement presentType = root.elementsByTagName(QString("ConnectionMode")).at(0).toElement();
    QDomElement newType = m_document.createElement("ConnectionMode");
    newType.appendChild(m_document.createTextNode(QString::number(m_connectType)));
    root.replaceChild(newType, presentType);

    m_xmlFile.resize(0);
    QTextStream stream(&m_xmlFile);
    stream << m_document.toString();
    emit connectTypeChanged();
    qDebug() << "Updated connection type";
}

void DiagSettings::setResetIP(int reset)
{
    m_resetIP = reset;
    QDomElement root = m_document.firstChildElement();
    QDomElement presentType = root.elementsByTagName(QString("ResetIP")).at(0).toElement();
    QDomElement newType = m_document.createElement("ResetIP");
    newType.appendChild(m_document.createTextNode(QString::number(m_resetIP)));
    root.replaceChild(newType, presentType);

    m_xmlFile.resize(0);
    QTextStream stream(&m_xmlFile);
    stream << m_document.toString();
    emit resetIPChanged();
    qDebug() << "Updated IP reset on exit";
}

QString DiagSettings::saveLog(QString fileName, QString text)
{
    fileName.remove("file:///");
    QFile file(fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream outStream(&file);
    outStream << text;
    file.close();
    return QFileInfo(fileName).absoluteFilePath();
}

void DiagSettings::setAppWidth(int width)
{
    m_appWidth = width;
}

void DiagSettings::setAppHeight(int height)
{
    m_appHeight = height;
}

DiagSettings::~DiagSettings()
{
    m_xmlFile.close();
}

QString DiagSettings::getHelpText()
{
    QString filename = QDir(qApp->applicationDirPath()).absoluteFilePath(HELP_FILE);
    QFile helpFile;
    helpFile.setFileName(filename);
    qDebug() << helpFile.fileName() << "helpfile exists";
    helpFile.open(QIODevice::ReadOnly | QIODevice::Text);
    QByteArray ba = helpFile.readAll();
    helpFile.close();
    return QString(ba);
}

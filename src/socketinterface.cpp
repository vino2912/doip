#include "inc/socketinterface.h"
#include <unistd.h>
#include <QNetworkInterface>

SocInterface::SocInterface(QObject *parent):
    QObject(parent), m_networkConnectionName("Local Area Connection")
{
    //Initialize variables
    hostIP["Local Area Connection"] = "0.0.0.0";
    m_ConnectionStatus = false;
    m_resetIPOnExit = false;
    m_presentIP["Local Area Connection"] = "";
    m_networkNameList.clear();

    Worker *worker = new Worker;
    worker->moveToThread(&workerThread);
    connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
    connect(this, &SocInterface::sendRequestSignal, worker, &Worker::sendRequest);
    connect(this, &SocInterface::sendSeatHeaterValueSignal, worker, &Worker::sendSeatHeaterValue);
    connect(this, &SocInterface::setTgtIPSignal, worker, &Worker::setTgtIP);
    connect(this, &SocInterface::setTesterAddressSignal, worker, &Worker::setTesterAddress);
    connect(this, &SocInterface::setDoIPNodeAddressSignal, worker, &Worker::setDoIPNodeAddress);
    connect(this, &SocInterface::setPortNumSignal, worker, &Worker::setPortNum);
    connect(this, &SocInterface::connectToTargetSignal, worker, &Worker::connectToTarget);
    connect(this, &SocInterface::disconnectFromTargetSignal, worker, &Worker::disconnectFromTarget);
    connect(this, &SocInterface::setWriteDIDValueSignal, worker, &Worker::setWriteDIDValue);
    connect(worker, &Worker::dataReceived, this, &SocInterface::dataReceived);
    connect(worker, &Worker::updateStatus, this, &SocInterface::setStatus);
    connect(worker, &Worker::connectionStatusChanged, this, &SocInterface::setConnectionStatus);
    workerThread.start();
    m_presentIP["Local Area Connection"] = getInterfaceIP();
    hostIP["Local Area Connection"] = m_presentIP["Local Area Connection"];
}

SocInterface::~SocInterface()
{
    disconnectFromTarget();

    if(m_resetIPOnExit && ("" != m_presentIP[m_networkConnectionName]))
    {
        QProcess *qProc = new QProcess(this);
        QString cmd = "netsh interface ip set address name=\"" + m_networkConnectionName + "\" static " + m_presentIP[m_networkConnectionName] + " 255.255.255.0";

        qProc->start(cmd);
        qProc->waitForFinished();

        QByteArray cmdRetVal = qProc->readAll();
        qDebug() << "cmd returned : " << cmdRetVal;
    }

    workerThread.quit();
    workerThread.wait();
}

QString SocInterface::sendSeatHeaterValue(QString value)
{
    emit sendSeatHeaterValueSignal(value);
    return QString("Sending seat heater setting: " + value);
}

QString SocInterface::sendRequest(QString request)
{
    emit sendRequestSignal(request);
    return QString("Sending request: " + request);
}

QString SocInterface::connectToTarget()
{
    emit connectToTargetSignal();
    return QString("Connecting...");
}

void SocInterface::disconnectFromTarget()
{
    emit disconnectFromTargetSignal();
}

bool SocInterface::checkIP(QString newIP)
{
    QHostAddress address(newIP);
    if (QAbstractSocket::IPv4Protocol == address.protocol())
    {
        qDebug("Valid IPv4 address.");
    }
    else if (QAbstractSocket::IPv6Protocol == address.protocol())
    {
        qDebug("Valid IPv6 address.");
    }
    else
    {
        qDebug("Unknown or invalid address.");
        return false;
    }
    return true;
}

QString SocInterface::setHostIP(QString newIP, QString networkConnectionName)
{
    qDebug() << "Inside setHostIP" << newIP << hostIP;

    if(!hostIP.contains(networkConnectionName)) {
        m_presentIP[networkConnectionName] = getInterfaceIP(networkConnectionName);
        hostIP[networkConnectionName] = m_presentIP[networkConnectionName];
    }

    if(hostIP[networkConnectionName] == newIP)
    {
        return "Host IP unchanged";
    }

    QHostAddress address(newIP);
    if (QAbstractSocket::IPv4Protocol == address.protocol())
    {
        qDebug("Valid IPv4 address.");
        hostIP[networkConnectionName] = newIP;
    }
    else if (QAbstractSocket::IPv6Protocol == address.protocol())
    {
        qDebug("Valid IPv6 address.");
        hostIP[networkConnectionName] = newIP;
    }
    else
    {
        qDebug("Unknown or invalid address.");
        return QString("Unknown or invalid address");
    }

    m_networkConnectionName = networkConnectionName;

    QProcess *qProc = new QProcess(this);
    QString cmd = "netsh interface ip set address name=\"" + networkConnectionName + "\" static " + newIP + " 255.255.255.0";

    qProc->start(cmd);
    qProc->waitForFinished();

    QByteArray cmdRetVal = qProc->readAll();
    qDebug() << "cmd returned : " << cmdRetVal;

    if(cmdRetVal.trimmed().isEmpty())
    {
        emit disconnectFromTargetSignal();
        return QString("Host IP Changed successfully");
    }
    else
    {
        return QString("Set Host IP failed: " + cmdRetVal.trimmed());
    }
}

QString SocInterface::setTgtIP(QString newIP)
{
    emit setTgtIPSignal(newIP);
    return QString("Updating target IP to: " + newIP);
}

QString SocInterface::setTesterAddress(QString sTesterAddress)
{
    emit setTesterAddressSignal(sTesterAddress);
    return QString("Setting Tester Address to: " + sTesterAddress);
}

QString SocInterface::setDoIPNodeAddress(QString sDoIPNodeAddr)
{
    emit setDoIPNodeAddressSignal(sDoIPNodeAddr);
    return QString("Setting DoIP Node Address to: " + sDoIPNodeAddr);
}

void SocInterface::getSystemIP()
{
    if("" == m_presentIP[m_networkConnectionName]) {
        emit updateStatus("Current Host IP could not be found; Check Ethernet cable connection and restart application");
    }
    else {
        emit updateStatus("Current Host IP : " + m_presentIP[m_networkConnectionName]);
    }
}

QString SocInterface::getInterfaceIP(const QString sName)
{
    QString networkName;
    QString ipAddr = "";
    if("" != sName) {
        networkName= sName;
    }
    else {
        networkName = m_networkConnectionName;
    }
    QProcess *qProc = new QProcess(this);
    QString cmd = "netsh interface ip show config name=\"" + networkName + "\"";
    qProc->start(cmd);
    qProc->waitForFinished();

    QByteArray cmdRetVal = qProc->readAll();

    //Extract IP
    QRegExp rx("IP Address:\\s*(\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3})");
    int pos = 0;

    while ((pos = rx.indexIn(QString(cmdRetVal), pos)) != -1)
    {
        ipAddr = rx.cap(1);
        pos += rx.matchedLength();
    }

    qProc->deleteLater();
    return ipAddr;
}

void SocInterface::getAvailableLANConnections()
{
    //Get available LAN connections
    QProcess *qProc = new QProcess(this);
    QString cmd = "ipconfig";
    qProc->start(cmd);
    qProc->waitForFinished();
    QByteArray cmdRetVal = qProc->readAll();
    QStringList sL = QString(cmdRetVal).split("\r\n");

    for(int loop = 0; loop < sL.length(); loop++) {

        QRegExp rx1("Ethernet adapter (Local Area Connection.*):");
        int pos = 0;

        while ((pos = rx1.indexIn(sL.at(loop), pos)) != -1)
        {
            if(rx1.cap(1) == "Local Area Connection") {
                m_networkNameList.insert(0, rx1.cap(1));
            }
            else {
                m_networkNameList.append(rx1.cap(1));
            }
            pos += rx1.matchedLength();
        }
    }
    qProc->deleteLater();
    emit networkNameListChanged(m_networkNameList);
}

void SocInterface::setPortNum(quint16 portNum, quint8 type)
{
    emit setPortNumSignal(portNum, type);
}

void SocInterface::setStatus(QString text)
{
    m_LastLog = text;
    emit updateStatus(text);
}
QString SocInterface::status()
{
    return m_LastLog;
}

void SocInterface::setConnectionStatus(bool status)
{
    m_ConnectionStatus = status;
    emit connectionStatusChanged(m_ConnectionStatus);
}

bool SocInterface::connectionStatus()
{
    return m_ConnectionStatus;
}

void SocInterface::setResetIP(int reset)
{
    m_resetIPOnExit = reset;
}

void SocInterface::setWriteDIDValue(QString str)
{
    emit setWriteDIDValueSignal(str);
}
